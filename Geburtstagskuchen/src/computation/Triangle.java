package computation;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Ein Dreieck, welches durch drei Knoten definiert ist
 *  
 * @author Niels
 *
 */

public class Triangle extends Polygon implements Cloneable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2970969661265723194L;
	
	public Triangle(int[] xCoords,int[] yCoords){
		this(xCoords, yCoords, 3);
	}
	
	public Triangle(int[] xCoords,int[] yCoords, int npoints){
		super(xCoords, yCoords, npoints);
		if(npoints != 3 || xCoords.length != 3 || yCoords.length != 3)
			throw new IllegalArgumentException("Ung�ltige Anzahl an Ecken");
	}
	
	public Triangle(Point2D point1, Point2D point2, Point2D point3){
		super();
		addPoint(point1);
		addPoint(point2);
		addPoint(point3);
	}
	
	public Triangle(Vertex vertex1, Vertex vertex2, Vertex vertex3){
		this(
			vertex1.getCoords(),
			vertex2.getCoords(),
			vertex3.getCoords()
			);
	}
	
	public Triangle(Collection<Vertex> v) {
		super();
		if(v.size() != 3)
			throw new IllegalArgumentException("Ung�ltige Anzahl an Ecken: " + v.size());
		for(Vertex vertex : v){
			addPoint(vertex.getCoords());
		}
	}

	public Triangle(Point2D[] p) {
		super();
		if(p.length != 3)
			throw new IllegalArgumentException("Ung�ltige Anzahl an Ecken: " + p.length);
		for(Point2D q : p){
			addPoint(q);
		}
	}

	/**
	 * F�gt dem Dreieck einen Punkt hinzu, falls es nicht bereits 3 Punkte hat
	 * @param q Der Punkt, der hinzugef�gt werden soll
	 */
	public void addPoint(Point2D q){
		addPoint((int) q.getX(),(int) q.getY());
	}
	
	/**
	 * F�gt dem Dreieck einen Punkt hinzu, falls es nicht bereits 3 Punkte hat
	 * @param xPoint X-Koordinate des Punktes
	 * @param yPoint Y-Koordinate des Punktes
	 */
	public void addPoint(int xPoint, int yPoint){
		if(super.npoints < 3){
			super.addPoint(xPoint, yPoint);
		}
	}
	
	/**
	 * Berechnet aus den Punkten des Polygons die Knoten dieses Dreiecks
	 * @return Die Eckknoten des Dreiecks
	 */
	public Set<Vertex> getVertices(){
		Set<Vertex> vertices = new HashSet<Vertex>();
		for(Point2D p : getPoints()){
			vertices.add(new Vertex(p));
		}
		return vertices;
	}
	
	/**
	 * Berechnet aus den Punkten des Polygons die Punkte dieses Dreiecks
	 * @return Die Eckpunkte des Dreeicks
	 */
	public Set<Point2D> getPoints(){
		Set<Point2D> points = new HashSet<Point2D>();
		for(int i = 0; i < super.npoints; i++){
			points.add(new Point2D.Double(
					super.xpoints[i],
					super.ypoints[i]
			));
		}
		return points;
	}
	
	/**
	 * Gibt die Fl�che des Dreiecks einheitslos zur�ck<br>
	 * <a href="https://de.wikipedia.org/wiki/Dreiecksfl%C3%A4che#Alle_drei_Seitenl.C3.A4ngen_gegeben">https://de.wikipedia.org/wiki/Dreiecksfl%C3%A4che#Alle_drei_Seitenl.C3.A4ngen_gegeben</a>
	 * @return
	 */
	public double getArea(){
		//Bestimme die Seiten des Dreiecks
		Set<Line2D> outline = getOutline();
		//Und dessen Umfang
		double perimeter = 0;
		for(Line2D l : outline){
			perimeter += Shape.segmentLength(l);
		}
		double semiperimeter = 0.5 * perimeter;
		//Sowie die Diskriminante der Formel
		double discriminant = semiperimeter;
		for(Line2D l : outline){
			discriminant *= semiperimeter - Shape.segmentLength(l);
		}
		//Gebe die Quadratwurzel der Diskriminante zur�ck
		return Math.sqrt(discriminant);	
	}
	
	/**
	 * Gibt zur�ck, ob dieses Dreieck den �bergebenen Kreis schneidet<br>
	 * Ist der Mittelpunkt des Kreises im Dreieck enthalten, so muss der Kreis das Dreieck schneiden.<br>
	 * Ansonsten wird der Abstand des Kreismittelpunktes vom Rand des Dreicks bestimmt. Ist dieser kleiner als der Radius
	 * des Kreises, muss der Kreis den Rand schneiden
	 * @param c Der in Frage kommende Kreis
	 * @return Schneidet dieses Dreieck den Kreis?
	 */
	public boolean intersects(Circle c){
		if(super.contains(c.getMidpoint().getCoords())) return true;
		for(Line2D l : getOutline()){
			if(l.ptSegDist(c.getMidpoint().getCoords()) < c.getRadius()){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gibt zur�ck, ob dieses Dreieck die Fl�che des anderen Dreicks schneidet
	 * @param t Ein Dreieck
	 * @return Schneiden sich die beiden Dreicke? (Dreicke innerhalb eines "Shape"-Objekts geben hier IMMER <code>true<code> zur�ck)
	 */
	public boolean intersects(Triangle t){
		//Falls sich die Seiten der Dreiecke schneiden
		for(Line2D l : t.getOutline()){
			for(Line2D m : this.getOutline()){
				if(m.intersectsLine(l)) return true;
			}
		}
		//Falls ein Punkt des anderen Dreicks in diesem Dreick liegt
		for(Point2D p : t.getPoints() ){
			if(super.contains(p)) return true;
		}
		//Oder ein Punkt dieses Dreiecks in dem anderen Dreieck liegt
		for(Point2D p : getPoints()){
			if(t.contains(p)) return true;
		}
		return false;
	}
	
	/**
	 * Gibt zur�ck, ob dieses Dreieck die Fl�che des anderen Dreiecks schneidet,
	 * au�er die sich schneidenden Seiten sind identisch (WithOut Equal Lines)
	 *  oder schneiden sich nur in einem Punkt
	 * @param t Ein Dreieck
	 * @return Schneiden sich die beiden Dreiecke ( au�er an Seiten die identisch sind)?
	 */
	public boolean intersectsWOEL(Triangle t){
		//Falls ein Punkt des anderen Dreiecks in diesem Dreieck liegt
		//und er nicht eine Ecke dieses Dreiecks ist
		for(Point2D p : t.getPoints()){
			if(super.contains(p) && !this.getPoints().contains(p)) return true;
		}
		//Oder ein Punkt dieses Dreiecks in dem anderen Dreieck liegt
		for(Point2D p : getPoints()){
			if(t.contains(p)  && !t.getPoints().contains(p)) return true;
		}
		//Falls sich die Seiten der Dreiecke schneiden
		//und der Schnittpunkt nicht im End- oder Startpunkt der Linien liegt
		for(Line2D l : t.getOutline()){
			for(Line2D m : this.getOutline()){
				if(	m.intersectsLine(l) && !Shape.linesCutEOS(l, m) ) 
					return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Gibt zur�ck ob dieses Dreieck den �bergebenen Knoten schneidet
	 * @param v
	 * @return
	 */
	public boolean contains(Vertex v){
		return super.contains(v.getCoords());
	}
	
	/**
	 * Gibt zur�ck, ob dieses Dreieck den �bergebenen Kreis enth�lt.<br>
	 * Dies ist der Fall, wenn der Mittelpunkt des Kreises in der Form liegt, 
	 * und der Kreis den Rand des Dreiecks nicht schneidet
	 * @param c Der in Frage kommende Kreis
	 * @return Enth�lt dieses Dreieck den Kreis?
	 */
	public boolean contains(Circle c){
		if(super.contains(c.getMidpoint().getCoords())){
			for(Line2D l : getOutline()){
				if(c.cuts(l)){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Gibt die �u�ere Form des Dreiecks in Form eines Sets von Linien wieder
	 * @return Ein Set, das alle Kanten des Dreiecks enth�lt
	 */
	public Set<Line2D> getOutline(){
		Set<Line2D> returnSet = new HashSet<Line2D>();
		Point2D[] points = this.getPoints().toArray(new Point2D[3]);
		returnSet.add(new Line2D.Double(
				points[0],
				points[1]));
		returnSet.add(new Line2D.Double(
				points[1],
				points[2]));
		returnSet.add(new Line2D.Double(
				points[2],
				points[0]));
		return returnSet;
	}
	
	/**
	 * Gibt den kleinsten Abstand vom Rand des Dreiecks zum Punkt wieder
	 * @param p Der Punkt zu dem der Abstand besteht
	 * @return Der kleinste Abstand zwischen Punkt und Rand des Dreiecks
	 */
	public double distanceToOutline (Point2D p){
		double minDist = Double.POSITIVE_INFINITY;
		// �berpr�fe f�r jede Kante des Dreiecks die Entfernung zum Punkt 
		// und gebe die kleinste zur�ck
		for(Line2D l : getOutline()){
			double dist = l.ptSegDist(p);
			if(dist < minDist){
				minDist = dist;
			}
		}
		return minDist;
	}
	
	/**
	 * Gibt den kleinsten Abstand vom Rand des Dreiecks zum Kreis wieder
	 * @param c Der Kreis zu dem der Abstand besteht
	 * @return Der kleinste Abstand zwischen Kreis und Rand des Dreiecks
	 */
	public double distanceToOutline (Circle c){
		return distanceToOutline(c.getMidpoint().getCoords()) - c.getRadius(); 
	}
	
	public boolean equals (Object o){
		if(o.getClass().equals(this.getClass()))
			return equals((Triangle) o);
		return false;
	}
	
	public boolean equals(Triangle t){
		return getPoints().containsAll(t.getPoints());
	}
	
	public String toString(){
		String returnS = "Triangle ";
		for(Point2D p : getPoints()){
			returnS += "[ " + (int) p.getX() + ", " + (int) p.getY() + " ]";
		}
		return returnS;
	}
	
	public static Set<Triangle> parseTriangles(String s){
		Set<Triangle> returnSet = new HashSet<Triangle>();
		if(s != null && !s.isEmpty()){
			Vector<String> strings = new Vector<String>();
			//Teile den String dort wo keine Zahlen sind
			strings.addAll(Arrays.asList(s.split("\\D+")));
			//Filtere alle leeren Strings heraus
			Vector<String> removeStrings = new Vector<String>();
			for(String t : strings){
				if(!t.matches("\\d+"))
					removeStrings.add(t);
			}
			strings.removeAll(removeStrings);
			//Reduziere die Gr��e des Vektors auf eine durch sechs teilbare Zahl (6 Koordinaten pro Dreieck)
			strings.setSize((strings.size() / 6) * 6);
			//Extrahiere aus dem Stringarray die enthaltenen Zahlen
			for(int i = 0; i <  strings.size(); i += 6){
				Integer[] specs = new Integer[6];
				for(int j = 0; j < specs.length; j++){
					specs[j] = (int) Integer.parseInt(strings.get(i+j));
				}
				Vertex[] vertices = new Vertex[3];
				for(int j = 0; j < vertices.length; j++){
					vertices[j] = new Vertex(specs[2*j], specs[2*j +1]);
				}
				returnSet.add(new Triangle(Arrays.asList(vertices)));
			}
		}
		return returnSet;
	}
	
	public Object clone(){
		return new Triangle(xpoints, ypoints, npoints);
	}
}
