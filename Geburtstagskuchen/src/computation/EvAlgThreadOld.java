package computation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Ein Thread, der den evolution�ren Algorithmus zur Ann�herung an eine optimale Verteilung durchf�hrt
 * @author Niels
 *
 */
public class EvAlgThreadOld extends Thread {
	
	/**
	 * Das Lager, in dem die neue Anordnung gespeichert wird
	 */
	private SynchronizedStore synchronizedStore;
	/**
	 * Die Zahl der Schritte, die durchlaufen werden (-1 = unendlich)
	 */
	private int steps;
	/**
	 * Die Versuche, die pro Schritt getan werden
	 */
	private int tries;
	
	/**
	 * Legt die Methode fest, nach der die Kreise bewegt werden sollen
	 */
	private int method;
	
	public EvAlgThreadOld(){
		this(Main.getSynchronizedStore());
	}
	
	public EvAlgThreadOld(SynchronizedStore synchronizedStore) {
		this.synchronizedStore = (SynchronizedStore) synchronizedStore.clone();
		setSteps(100);
		setTries(100);
		setMethod(3);
	}
	
	/**
	 * Legt die Anzahl der Schritte fest
	 * @param number
	 */
	public void setSteps(int number){
		if(!this.isAlive()){
			steps = number;
		}
	}
	
	/**
	 * Legt die Zahl an Versuchen pro Schritt fest
	 * @param number
	 */
	public void setTries(int number){
		if(!this.isAlive()){
			tries = number;
		}
	}
	
	@Override
	public void run() {
		if(!synchronizedStore.getCircles().isEmpty() && synchronizedStore.getShape() != null && !synchronizedStore.getShape().getTriangles().isEmpty()){
			for(int i = 0; i < steps && !Thread.interrupted(); i++){
				//F�hre x verschiedene Versuche zur Verschiebung der Kreise aus
				Trie[] steps = new Trie[tries];
				for(int j = 0; j < steps.length; j++){
					switch(method){
					case 1:
						steps[j] = new Trie.TrieAll(synchronizedStore);
						break;
					case 2:
						steps[j] = new Trie.TrieShortest(synchronizedStore);
						break;
					case 3:
						if(i%2 > 0)
							steps[j] = new Trie.TrieAll(synchronizedStore);
						else
							steps[j] = new Trie.TrieShortest(synchronizedStore);
						break;
					/*case 4:
						for(int k = 0; k < 360; k +=2){
							for(int l = 0; l < 100; l++){
								double angle = Math.toRadians(k);
								double distance = l/100 * maxDistance;
								steps[j] = new Trie.TrieOptimal(synchronizedStore);
							}
						}*/
					}
					steps[j].start();
				}
				//Vergleiche die Minimaldistanzen und �berpr�fe ob eine gr��er als bisher entstanden ist
				double minDistance = synchronizedStore.minimalDistance();
				SynchronizedStore bestStore = synchronizedStore;
				for(Trie s : steps){
					try {
						s.join();
					} catch (InterruptedException e) {
						break;
					}
						double newDist = s.getSynchronizedStore().minimalDistance();
						if(newDist > minDistance){
							bestStore = s.getSynchronizedStore();
							minDistance = newDist;
						}
				}
				setSynchronizedStore(bestStore);
				System.out.println(bestStore.computeRegularity());
			}
		}
	}
	
	public synchronized SynchronizedStore getSynchronizedStore() {
		return (SynchronizedStore) synchronizedStore.clone();
	}
	
	public synchronized void setSynchronizedStore(SynchronizedStore newStore){
		synchronizedStore = (SynchronizedStore) newStore.clone();
	}
	
	/**
	 * Legt die Methode fest, nach der die Kreise verteilt werden soll
	 * @param method Der Index der Methode 
	 * <ol><li>Bewege alle Kreise zuf�llig</li>
	 * <li>Bewege nur den Kreis, der die geringste Distanz zu anderen Punkten oder dem Rand hat</li>
	 * <li>Verwende die obigen Methoden abwechselnd</li>
	 * </ol>
	 */
	public void setMethod(int method) {
		if(!isAlive()){
			int[] poss = {1,2,3};
			for(int i : poss){
				if(method == i){
					this.method = method;
					return;
				}
			}
			throw new IllegalArgumentException("Ung�ltige Methodennummer");
		}
	}
	
	/*

	/**
	 * Ein Versuch, dessen Ergebnis ein Store ist, in dem neue (m�glichst gleichm��gere) Kreise liegen
	 * @author Niels
	 *
	 */
	public static class Trie extends Thread {
		
		protected SynchronizedStore synchronizedStore;
		protected double newMinDistance;
		
		public SynchronizedStore getSynchronizedStore() {
			return synchronizedStore;
		}

		public Trie(){
			this(Main.getSynchronizedStore());
		}
		
		public Trie(SynchronizedStore synchronizedStore){
			this.synchronizedStore = (SynchronizedStore) synchronizedStore.clone();
			newMinDistance = synchronizedStore.minimalDistance();
		}

				
		public boolean moveInRndDir (Circle c, double maxDistance){
			double distance = Math.random() * maxDistance;
			double angle = Math.toRadians(Math.random() * 360);
			return moveInDir(c, angle, distance);
		}
		
		public boolean moveInDir(Circle c, double angle, double distance){
			Point2D oldCoords = c.getMidpoint().getCoords();
			Point2D newCoords = c.getMidpoint().pointInDir(angle, distance);
			return synchronizedStore.moveCircle(oldCoords, newCoords);
		}
		/**
		 * Eine Version des Versuches, bei der ALLE Kreise zuf�llig bewegt werden
		 * @author Niels
		 *
		 */
		public static class TrieAll extends Trie {
			
		public TrieAll(){
			super();
		}
		
		public TrieAll(SynchronizedStore synchronizedStore){
			super(synchronizedStore);
		}
			
		@Override
			public void run() {
				double maxDistance = synchronizedStore.optimalDistance() - newMinDistance;
				for(Circle c : synchronizedStore.getCircles()){
					boolean success = false;
					do{
						success = moveInRndDir(c, maxDistance);
					}while(!success && !Thread.interrupted());
				}
				newMinDistance = synchronizedStore.minimalDistance();
			}
				
		}
		
		public static class TrieShortest extends Trie {
			public TrieShortest(){
				super();
			}
			
			public TrieShortest(SynchronizedStore synchronizedStore){
				super(synchronizedStore);
			}
				
			@Override
			public void run() {
				double maxDistance = synchronizedStore.optimalDistance() - newMinDistance;
				Line2D shortestDistance = synchronizedStore.shortestConnectingLine();
				Circle circleWithShortestDistance = synchronizedStore.cuttingCircle(shortestDistance.getP2());
				boolean success = false;
				do{
					success = moveInRndDir(circleWithShortestDistance, maxDistance);
				}while(!success && !Thread.interrupted());
				newMinDistance = synchronizedStore.minimalDistance();
			}
			
		}
		
		public static class TrieOptimal extends Trie {
			
			private double distance;
			private double angle;
			
			public TrieOptimal(){
				this(Main.getSynchronizedStore());
			}
			
			public TrieOptimal(SynchronizedStore synchronizedStore){
				this(synchronizedStore, 0, 0);
			}
			
			public TrieOptimal(SynchronizedStore synchronizedStore, double angle, double distance){
				super(synchronizedStore);
				setAngle(angle);
				setDistance(distance);
			}
			
			public void run(){
				Line2D shortestDistance = synchronizedStore.shortestConnectingLine();
				Circle circleWithShortestDistance = synchronizedStore.cuttingCircle(shortestDistance.getP2());
				moveInDir(circleWithShortestDistance, angle, distance);
				newMinDistance = synchronizedStore.minimalDistance();
			}

			public double getAngle() {
				return angle;
			}

			public void setAngle(double angle) {
				this.angle = angle;
			}

			public double getDistance() {
				return distance;
			}

			public void setDistance(double distance) {
				this.distance = distance;
			}
		}
	}
	
}
