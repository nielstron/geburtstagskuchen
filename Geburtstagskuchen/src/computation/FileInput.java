package computation;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This program reads a text file line by line and print to the console. It uses
 * FileInputStream to read the file.
 * Slightly amended by Niels M�ndler
 * Origin: <a href="http://www.java-tips.org/java-se-tips-100019/18-java-io/2028-how-to-read-file-in-java.html">http://www.java-tips.org/java-se-tips-100019/18-java-io/2028-how-to-read-file-in-java.html</a>
 * @author Java Tips 
 */

public class FileInput {


	  public static String fileToString(File f){

	    File file = f;
	    FileInputStream fis = null;
	    BufferedInputStream bis = null;
	    BufferedReader br = null;
	    String output = "";

	    try {
	      fis = new FileInputStream(file);

	      // Here BufferedInputStream is added for fast reading.
	      bis = new BufferedInputStream(fis);
	      br = new BufferedReader(new InputStreamReader(bis));

	      // dis.available() returns 0 if the file does not have more lines.
	      while (br.ready()) {

	      // this statement reads the line from the file and print it to
	        // the console.
	        output += br.readLine();
	      }

	      // dispose all the resources after using them.
	      fis.close();
	      bis.close();
	      br.close();

	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    
	    return output;
	  }
}
