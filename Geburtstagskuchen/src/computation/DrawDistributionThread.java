package computation;


import javax.swing.JComponent;

/**
 * Ein Thread, der regelm��ig den �bergebenen Ver�nderungsthread �berpr�ft und Panels updatet, die den aktuellen Fortschritt darstellen
 * @author nielstron
 *
 */
public class DrawDistributionThread extends Thread {
	
	private EvAlgThread evAlg;
	private JComponent[] components;
	
	public DrawDistributionThread (EvAlgThread evAlg, JComponent...components ){
		this.evAlg = evAlg;
		if(components == null) throw new IllegalArgumentException("Keine JKomponenten �bergeben");
		this.components = components;
		this.start();
	}
	
	public void run(){
		evAlg.start();
		while(evAlg.isAlive()){
			try {
				Thread.sleep(50);
				SynchronizedStore tmp = (SynchronizedStore) evAlg.getSynchronizedStore();
				Main.getSynchronizedStore().copyFrom(tmp);
				repaintComponents();
				
			} catch (InterruptedException e) {
				evAlg.interrupt();
				return;
			}
		}
	}
	
	private void repaintComponents(){
		for(JComponent jc : components){
			jc.repaint();
		}
	}
	
}
