package computation;

import java.awt.EventQueue;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import data.DataReference;
import gui.GuiThread;

public class Main {
	
	private static GuiThread guiThread = new GuiThread();
	private static SynchronizedStore synchronizedStore = new SynchronizedStore();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		//-t startet einen Testlauf
		if(args.length > 0 && args[0].equals("-t")){
			//Standarteinstellungen f�r einen Testlauf:
			SynchronizedStore syncstore = getSynchronizedStore();
			int steps = 1000;
			int tries = 100;
			int distributionMethod = 3;
			boolean silentMutation = false;
			//Setze als Anfangsform und Kreise die Werte aus der Aufgabenstellung
			try {
				URL url = DataReference.class.getResource("herzform.txt");
				File heartFile = new File(url.toURI());
				String heartString = FileInput.fileToString(heartFile);
				Set<Triangle> triangles = Triangle.parseTriangles(heartString);
				Main.getSynchronizedStore().getShape().getTriangles().addAll(triangles);
			} catch (URISyntaxException e1 ) {
				//unwichtig, dann gibt es eben keine Standartform
			}
			try{
				URL url = DataReference.class.getResource("kreispositionen.txt");
				File circleFile = new File(url.toURI());
				String circleString = FileInput.fileToString(circleFile);
				Set<Circle> circles = Circle.parseCircles(circleString);
				Main.getSynchronizedStore().setCircels(circles);
			} catch (URISyntaxException e1) {
				//unwichtig, dann gibt es eben keine Standartkreise
			}
			//Auslesen der �bergebenen Optionen
			List<String> arguments = Arrays.asList(args);
			try{
				for(String arg : arguments){
					if(arg.matches("steps=.*")) steps = Integer.parseInt(arg.substring(6));
					if(arg.matches("tries=.*")) tries = Integer.parseInt(arg.substring(6));
					if(arg.matches("distribution=.*")) tries = Integer.parseInt(arg.substring(14));
					if(arg.matches("silentMutation=.*")) silentMutation = Boolean.parseBoolean(arg.substring(16));
				}
			}
			catch(NumberFormatException e){
				System.out.println("Invalid number formatting");
				return;
			}
			//Start des Evolution�ren Algorithmus (Produziert Output von Selbst)
			EvAlgThread evAlg = new EvAlgThread(syncstore, steps, tries, distributionMethod, silentMutation);
			evAlg.run();
			try {
				evAlg.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//Ansonstens startet das Programm ganz normal
		else{
			EventQueue.invokeLater(new GuiThread());
		}
	}

	public GuiThread getGuiThread() {
		return guiThread;
	}

	/**
	 * Gibt das Lager f�r die Positionen der Kreise und der Form aus<br>
	 * <b>Wird dieses Lager ver�ndert, ist die ver�nderung dauerhaft!</b>
	 * @return Das globale Lager f�r die Position der Kreise und der Form
	 */
	public static SynchronizedStore getSynchronizedStore() {
		return synchronizedStore;
	}



}