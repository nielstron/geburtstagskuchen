package computation;

import java.util.HashSet;
import java.util.Set;

import javax.swing.JComponent;

/**
 * Schickt ein PropertyChange Event an alle �bergebenen Komponenten falls sich die Aktivit�t des Threads �ndert
 * @author Niels
 *
 */
public class ThreadMonitor extends Thread {
	
	private Thread thread;
	private boolean threadStatus;
	private Set<JComponent> jComponents;
	
	public ThreadMonitor(Thread thread, JComponent... jComponents) {
		setThread(thread);
		this.jComponents = new HashSet<JComponent>();
		addComponents(jComponents);
		threadStatus = false;
		start();
	}
	
	public void run(){
		while(!Thread.interrupted()){
			if(thread != null){
				FireThread fire = new FireThread();
				try {
					fire.join();
				} catch (InterruptedException e) {
					fire.interrupt();
					break;
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				break;
			}
		}
	}
	
	public void setThread(Thread newThread){
		thread = newThread;
	}
	
	public void addComponent(JComponent jc){
		jComponents.add(jc);
	}
	
	public void addComponents(JComponent... jComponents){
		if(jComponents != null){
			for(JComponent jc : jComponents){
				addComponent(jc);
			}
		}
	}
	
	public void removeComponent(JComponent jc){
		jComponents.remove(jc);
	}
	
	public void removeCompontents(JComponent... jComponents ){
		for(JComponent jc : jComponents){
			removeComponent(jc);
		}
	}
	
	public boolean getThreadStatus(){
		return threadStatus;
	}
	
	
	private class FireThread extends Thread {
		
		public FireThread(){
			this.start();
		}
		
		public void run(){
			while(!Thread.interrupted()){
				try {
					if(threadStatus != thread.isAlive()){
						threadStatus = thread.isAlive();
						for(JComponent jc : jComponents){
							 jc.firePropertyChange("activityIndicator", !threadStatus , threadStatus);
						}
					}
					Thread.sleep(100);
				} catch (InterruptedException e) {
					break;
				}
			}
		}
		
	}
	
}
