package computation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Ein Thread, der den evolution�ren Algorithmus zur Ann�herung an eine optimale Verteilung durchf�hrt
 * @author Niels
 *
 */
public class EvAlgThread extends Thread {
	
	/**
	 * Das Lager, in dem die neue Anordnung gespeichert wird
	 */
	private SynchronizedStore synchronizedStore;
	/**
	 * Die Zahl der Schritte, die durchlaufen werden (-1 = unendlich)
	 */
	private int steps;
	/**
	 * Die Zahl der Schritte die bereits durchlaufen wurden
	 */
	private int doneSteps;
	/**
	 * Die Versuche, die pro Schritt getan werden
	 */
	private int tries;
	
	/**
	 * Legt die Methode fest, nach der die Kreise bewegt werden sollen
	 */
	private int method;
	
	/**
	 * Der Zahlencode f�r die Kreisverteilungsmethode, bei der alle Kreise zuf�llig bewegt werden
	 */
	public static final int ALL_CIRCLES_METHOD = 1;
	
	/**
	 * Der Zahlencode f�r die Kreisverteilungsmethode, bei der nur der Kreis mit dem Minimalabstand bewegt wird
	 */
	public static final int MIN_CIRCLE_METHOD = 2;
	
	/**
	 * Der Zahlencode f�r die Kreisverteilungsmethode, bei der die beiden anderen Zufallsmethoden abwechselnd verwendet werden
	 */
	public static final int BOTH_METHODS = 3;
	
	/**
	 * Der Zahlencode f�r die Kreisverteilungsmethode, bei der nur der Kreis mit dem Minimalabstand m�glichst optimal bewegt wird
	 */
	public static final int OPT_MIN_CIRCLE_METHOD = 4;
	
	/**
	 * Wird stille Mutation (Ver�nderung ohne direkte Auswirkung) erlaubt?
	 */
	private boolean silentMutation;

	/**
	 * Hat ein Versuch nur ein besseres Ergebnis, wenn die neue Minimaldistanz geringer ist? (true) Oder reicht ein besserer Durchschnittsabstand?
	 */
	private boolean onlyMinDistance;
	
	/**
	 * Die Gleichm��igkeit der aktuelle Verteilten Kreise
	 */
	private double computedRegularity;
	
	/**
	 * Ein Array, dass alle m�glichen Methodenindices enth�lt<br>
	 * <ol><li>Bewege alle Kreise zuf�llig</li>
	 * <li>Bewege nur den Kreis, der die geringste Distanz zu anderen Punkten oder dem Rand hat</li>
	 * <li>Verwende die obigen Methoden gemischt</li>
	 * <li>Suche nach einer optimalen L�sung</li>
	 * </ol>
	 */
	private static final int[] METHODS = { 1, 2, 3, 4 };
	
	public EvAlgThread(){
		this(Main.getSynchronizedStore());
	}
	
	public EvAlgThread(SynchronizedStore synchronizedStore) {
		this(synchronizedStore, 1000, 100);
	}
	
	public EvAlgThread(SynchronizedStore synchronizedStore, int steps, int tries){
		this(synchronizedStore, steps, tries, 2);
	}
	
	public EvAlgThread(SynchronizedStore synchronizedStore, int steps, int tries, int method){
		this(synchronizedStore, steps, tries, method, true);
	}
	
	/**
	 * Erzeugt einen Thread, der einen Evolution�ren Algorithmus zur Verteilung von Kreisen im �bergebenen Store durchf�hrt
	 * @param synchronizedStore
	 * @param steps
	 * @param tries
	 * @param method
	 * @param silentMutation
	 */
	public EvAlgThread(SynchronizedStore synchronizedStore, int steps, int tries, int method, boolean silentMutation){
		this(synchronizedStore, steps, tries, method, silentMutation, true);
	}
	
	/**
	 * Erzeugt einen Thread, der einen Evolution�ren Algorithmus zur Verteilung von Kreisen im �bergebenen Store durchf�hrt
	 * @param synchronizedStore
	 * @param steps
	 * @param tries
	 * @param method
	 * @param silentMutation
	 */
	public EvAlgThread(SynchronizedStore synchronizedStore, int steps, int tries, int method, boolean silentMutation, boolean onlyMinDistance){
		this.synchronizedStore = (SynchronizedStore) synchronizedStore.clone();
		computedRegularity = synchronizedStore.computeRegularity();
		setSteps(steps);
		setTries(tries);
		setMethod(method);
		setSilentMutation(silentMutation);
		setOnlyMinDistance(onlyMinDistance);
		doneSteps = 0;
	}
	
	
	/**
	 * Legt die Anzahl der Schritte fest
	 * @param number
	 */
	public void setSteps(int number){
		if(!this.isAlive()){
			steps = number;
		}
	}
	
	/**
	 * Legt fest ob stille Mutation erlaubt wird
	 * @param allow Wird stille Mutation erlaubt?
	 */
	public void setSilentMutation(boolean allow){
		if(!this.isAlive()){
			silentMutation = allow;
		}
	}
	
	/**
	 * Legt die Zahl an Versuchen pro Schritt fest
	 * @param number
	 */
	public void setTries(int number){
		if(!this.isAlive()){
			tries = number;
		}
	}
	
	@Override
	public void run() {
		if(!synchronizedStore.getCircles().isEmpty() && synchronizedStore.getShape() != null && !synchronizedStore.getShape().getTriangles().isEmpty()){
			for(int i = 0; i < steps || steps < 0; i++){
				
				//Erzeuge eine neue Versuchsreihe
				TrieRow row = new TrieRow(tries, getSynchronizedStore(), method, silentMutation, onlyMinDistance);
				try{
					row.join();
				} catch(InterruptedException e){
					break;
				}
				setSynchronizedStore(row.getSynchronizedStore());
				
				doneSteps = i;
				double regularity = row.getSynchronizedStore().computeRegularity();
				computedRegularity = regularity;
				System.out.println(regularity);
			}
		}
	}
	
	public synchronized SynchronizedStore getSynchronizedStore() {
		return (SynchronizedStore) synchronizedStore.clone();
	}
	
	public synchronized void setSynchronizedStore(SynchronizedStore newStore){
		synchronizedStore = (SynchronizedStore) newStore.clone();
	}
	
	/**
	 * Legt die Methode fest, nach der die Kreise verteilt werden sollen
	 * @param method Der Index der Methode 
	 * <ol><li>Bewege alle Kreise zuf�llig</li>
	 * <li>Bewege nur den Kreis, der die geringste Distanz zu anderen Punkten oder dem Rand hat</li>
	 * <li>Verwende die obigen Methoden gemischt</li>
	 * <li>Suche nach einer optimalen L�sung</li>
	 * </ol>
	 */
	public void setMethod(int method) {
		if(!isAlive()){
			for(int i : METHODS){
				if(method == i){
					this.method = method;
					return;
				}
			}
			throw new IllegalArgumentException("Ung�ltige Methodennummer:" + method);
		}
	}
	
	
	
	
	
	
	public boolean isOnlyMinDistance() {
		return onlyMinDistance;
	}
	
	/**
	 * Legt fest ob ein Versuch nur besser ist, wenn die Minimaldistanz besser ist (true) oder auch wenn die Durchschnittsdistanz besser ist (false).
	 * @param onlyMinDistance
	 */
	public void setOnlyMinDistance(boolean onlyMinDistance) {
		if(!this.isAlive()){
			this.onlyMinDistance = onlyMinDistance;
		}
	}

	/**
	 * Gibt die Zahl der Schritte zur�ck, die bereits getan wurden
	 * @return
	 */
	public int getDoneSteps() {
		return doneSteps;
	}
	
	/**
	 * Gibt die aktuelle Gleichverteilung der Kreise zur�ck
	 * @return
	 */
	public double getComputedRegularity(){
		return computedRegularity;
	}






	/**
	 * Eine Versuchsreihe, die eine bestimmte Anzahl an Versuchen durchf�hrt und das beste Ergebnis speichert
	 * @author Niels
	 *
	 */
	public static class TrieRow extends Thread {
		
		/**
		 * Die Anzahl der Versuche, die durchgef�hrt werden sollen
		 */
		private int numberTries;
		
		/**
		 * Der Index der zu verwendenden Methode<br>
		 * Siehe {@link EvAlgThread.methods} f�r die Zahl der M�glichkeiten
		 */
		private int method;
		
		/**
		 * Der Store, in dem die aktuelle Postition der Kreise und der Form gespeichert wird
		 */
		private SynchronizedStore synchronizedStore;
		
		/**
		 * Der kleinste Abstand zwischen Kreis und Rand, der bisher erreicht wurde
		 */
		private double newMinDistance;
		
		/**
		 * Die Strecke, um die ein Kreis maximal bewegt werden darf
		 * Diese wird im Moment als vervierfachte Differenz zwischen Minimal- und Optimaldistanz
		 */
		private double maxMoveDistance;
		
		/**
		 * Der Kreis, bei dem sich der k�rzeste Abstand zum Rand oder einem anderen Kreis befindet
		 */
		private Circle minDistanceCircle;
		
		/**
		 * Wird stille Mutation (Ver�nderung ohne direkte Auswirkung) erlaubt?
		 */
		private boolean silentMutation;
		
		/**
		 * Hat ein Versuch nur ein besseres Ergebnis, wenn die neue Minimaldistanz geringer ist? (true) Oder reicht ein besserer Durchschnittsabstand?
		 */
		private boolean onlyMinDistance;
		
		/**
		 * Erzeugt eine Versuchreihe ohne Versuche
		 */
		public TrieRow (){
			this(0);
		}
		
		/**
		 * Erzeugt eine Versuchsreihe mit dem Globale Syncstore der Main-Klasse
		 * @param number Die Zahl der anzuwendenen Versuche 
		 */
		public TrieRow(int number){
			this(number, Main.getSynchronizedStore());
		}
		
		/**
		 * Erzeugt eine Versuchsreihe mit dem �bergebenen Lager und der �bergebenen Zahl an Versuchen
		 * @param number Die Zahl der anzuwendenen Versuche 
		 * @param store  Das Lager auf das die Versuche angewendet werden
		 */
		public TrieRow(int number, SynchronizedStore store){
			this(number, store, 2);
		}
		
		/**
		 * Eine Versuchsreihe, die die zu verwendende Methode auf die �bergebene Anzahl von Versuchen im �bergebenen Lager ausf�hrt<br>
		 * Stille Mutation wird nicht erlaubt<br>
		 * Es z�hlt nur eine bessere Minimaldistanz
		 * @param number Die Zahl der anzuwendenen Versuche 
		 * @param store Das Lager auf das die Versuche angewendet werden
		 * @param method Der Index der Methode die angewendet werden soll
		 */
		public TrieRow(int number, SynchronizedStore store, int method){
			this(number, store, method, false);
		}
		
		/**
		 * Eine Versuchsreihe, die die zu verwendende Methode auf die �bergebene Anzahl von Versuchen im �bergebenen Lager ausf�hrt<br>
		 * Es z�hlt nur eine bessere Minimaldistanz
		 * @param number Die Zahl der anzuwendenen Versuche 
		 * @param store Das Lager auf das die Versuche angewendet werden
		 * @param method Der Index der Methode die angewendet werden soll
		 * @param silentMutation  Wird stille Mutation (Ver�nderung ohne direkte Auswirkung) erlaubt?
		 */
		public TrieRow(int number, SynchronizedStore store, int method, boolean silentMutation){
			this(number, store, method, silentMutation, true);
		}
		
		/**
		 * Eine Versuchsreihe, die die zu verwendende Methode auf die �bergebene Anzahl von Versuchen im �bergebenen Lager ausf�hrt
		 * @param number Die Zahl der anzuwendenen Versuche 
		 * @param store Das Lager auf das die Versuche angewendet werden
		 * @param method Der Index der Methode die angewendet werden soll
		 * @param silentMutation  Wird stille Mutation (Ver�nderung ohne direkte Auswirkung) erlaubt?
		 * @param onlyMinDistance Hat ein Versuch nur ein besseres Ergebnis, wenn die neue Minimaldistanz geringer ist? (true) Oder reicht ein besserer Durchschnittsabstand?
		 */
		public TrieRow(int number, SynchronizedStore store, int method, boolean silentMutation, boolean onlyMinDistance){
			if(store.getCircles().isEmpty()) return;
			numberTries = number;
			//Muss zuvor gesetzt werden, da sonst ein falscher wert f�r den minimalwert gesetzt werden kann
			this.onlyMinDistance = onlyMinDistance;
			setSynchronizedStore(store);
			this.method = method;
			
			//Berechne einmal f�r den gesamten Lauf die gleichbleibenden Variablen
			//Problem des folgenden:
			//K�nnte ein Kreis an v�llig anderer Stelle gut passen, wird ihm die M�glichkeit verwehrt dort hin zu gelangen
			//Wird allerdings durch Vermischen von "alle Kreise bewegen" und "einen Kreis bewegen" ausgeglichen
			
			//Um sich an irgendeinem Wert zu orientieren, der die Bewegung der Kreise einschr�nkt wird die Optimaldistanz verwendet
			//Da der beste PLatz f�r den Kreis jedoch oft deutlich weiter entfernt ist als nur diese eine Distanz, 
			//oder sogar nur die Differenz zwischen Optimal- und Minimaldistanz wird die Differenz vervierfacht. So ergeben sich deutlich
			//bessere Ergebnisse
			maxMoveDistance = (synchronizedStore.optimalDistance() - synchronizedStore.minimalDistance()) * 4;//+ synchronizedStore.minimalDistance();
			if(method > 1){
				Line2D shortestDistance = synchronizedStore.shortestConnectingLine();
				minDistanceCircle = synchronizedStore.cuttingCircle(shortestDistance.getP2());
			}else minDistanceCircle = null;
			this.silentMutation = silentMutation;
			start();
		}
		
		public void run(){
			//F�hre numberTries verschiedene Versuche zur Verschiebung der Kreise aus
			Trie[] row = new Trie[numberTries];
			for(int j = 0; j < row.length; j++){
				switch(method){
				case 1:
					row[j] = new TrieAll();
					break;
				case 2:
					row[j] = new TrieMinimal();
					break;
				case 3:
					if(j%2 > 0)
						row[j] = new TrieAll();
					else
						row[j] = new TrieMinimal();
					break;
				case 4:
					//In diesem Fall bedeutet die Zahl der Versuche die genauigkeit mit der die maximaldistanz durchprobiert wird
					row[j] = new TrieOptimal(j+1, row.length);
					break;
				}
				row[j].start();
			}
			//Vergleiche die Minimaldistanzen oder Durchschnittsdistanzen und �berpr�fe ob eine gr��ere als bisher entstanden ist
			double minDistance = newMinDistance;
			SynchronizedStore bestStore = synchronizedStore;
			for(Trie s : row){
				try {
					s.join();
					double newDist = computeMinimalDistance(s.getNewSynchronizedStore());
					if(newDist > minDistance || (silentMutation && newDist == minDistance)){
				//if(newDist > minDistance || (newDist == minDistance && s.getNewSynchronizedStore().minimalDistance(minDistanceCircle) > bestStore.minimalDistance(minDistanceCircle))){
	//Die untere Abfrage (if) w�re eine M�glichkeit, den bewegten Kreis m�glichst weit von allen anderen Kreis entfernt zu platzieren
						bestStore = s.getNewSynchronizedStore();
						minDistance = newDist;
					}
				} catch (InterruptedException e) {
					break;
				}
			}
			setSynchronizedStore(bestStore);
		}
		
		/**
		 * Gibt einen Klon des Lagers f�r die Postitionen von Form und Kreisen aus
		 * @return
		 */
		public SynchronizedStore getSynchronizedStore() {
			return (SynchronizedStore) synchronizedStore.clone();
		}
		
		/**
		 * Setzt einen neues Lager als Lager f�r diesen Versuch und speichert dabei auch gleich noch dessen Minimalabstand
		 * @param synchronizedStore
		 */
		private void setSynchronizedStore(SynchronizedStore synchronizedStore){
			this.synchronizedStore = (SynchronizedStore) synchronizedStore.clone();
			newMinDistance = computeMinimalDistance(synchronizedStore);
		}
		
		/**
		 * Gibt den Kreis zur�ck, der die k�rzeste Distanz zu einem anderen Kreis oder dem Rand aufweist
		 * @return
		 */
		public Circle getMinDistanceCircle() {
			return minDistanceCircle;
		}
		
		/**
		 * Bestimmt die in diesem Fall wichtige Minimaldistanz oder Durchschnittsdistanz der Kreise
		 * @return
		 */
		private double computeMinimalDistance(SynchronizedStore s) {
			if(onlyMinDistance)
				return s.minimalDistance();
			else
				return s.averageDistance();
			// return s.minimalDistance() * 3 + s.averageDistance(); Alternativ k�nnten diese beiden werte miteinander verrechnet werden
		}

		/**
		 * Gibt die Distanz zur�ck, um die ein Kreis beie einem Versuch maximal bewegt werden darf
		 * @return
		 */
		public double getMaxMoveDistance() {
			return maxMoveDistance;
		}

		/**
		 * Ein Versuch, dessen Ergebnis ein Store ist, in dem neue (m�glichst gleichm��igere) Kreise liegen
		 * @author Niels
		 *
		 */
		public abstract class Trie extends Thread {
			
			protected SynchronizedStore newSynchronizedStore;
			
			public SynchronizedStore getNewSynchronizedStore() {
				if(!isAlive())
					return newSynchronizedStore;
				else return null;
			}
			
			public void setNewSynchronizedStore(SynchronizedStore store){
				newSynchronizedStore = (SynchronizedStore) store.clone();
			}

			public Trie(){
				setNewSynchronizedStore(getSynchronizedStore());
			}

					
			/**
			 * Bewegt den �bergebenen Kreis in eine zuf�llige Richtung
			 * @param c Der zu bewegende Kreis
			 * @param maxDistance Die Strecke, die der Kreis maximal zur�cklegen darf
			 * @return Wurde der Kreis an eine g�ltige Position verschoben?
			 */
			public boolean moveInRndDir (Circle c, double maxDistance){
				double distance = Math.random() * maxDistance;
				double angle = Math.toRadians(Math.random() * 360);
				return moveInDir(c, angle, distance);
			}
			
			/**
			 * Bewegt den �bergebenen Kreis im �bergebenen Winkel �ber die �bergebene Distanz
			 * @param c Der zu bewegende Kreis
			 * @param angle Der Winkel, in dem der Kreis bewegt wird
			 * @param distance Die Strecke, die der Kreis zur�cklegen soll
			 * @return  Wurde der Kreis an eine g�ltige Position verschoben?
			 */
			public boolean moveInDir(Circle c, double angle, double distance){
				Point2D oldCoords = c.getMidpoint().getCoords();
				Point2D newCoords = c.getMidpoint().pointInDir(angle, distance);
				return newSynchronizedStore.moveCircle(oldCoords, newCoords);
			}
			
		}
		
		/**
		 * Eine Version des Versuches, bei der ALLE Kreise zuf�llig bewegt werden
		 * @author Niels
		 *
		 */
		public class TrieAll extends Trie {
			
			public TrieAll(){
				super();
			}
			
			@Override
			public void run() {
				for(Circle c : newSynchronizedStore.getCircles()){
					boolean success = false;
					while(!success)
						success = moveInRndDir(c, getMaxMoveDistance());
				}
			}
				
		}
		
		public class TrieMinimal extends Trie {
			
			public TrieMinimal() {
				super();
			}
				
			@Override
			public void run() {
				boolean success = false;
				while(!success)
					success = moveInRndDir(getMinDistanceCircle(), getMaxMoveDistance());
			}
			
		}
		
		/**
		 * Eine eigene Reihe von versuchen, die f�r den �bergebenen anteil an der gesamt distanz jeden m�glichen winkel (oder m�glichst
		 * viele Winkel) ausprobiert
		 * @author Niels
		 *
		 */
		public class TrieOptimal extends Trie {
			
			/**
			 * Der Anteil an der insgesamt erlaubten Distanz, die der Kreis zur�cklegen soll
			 */
			private double partOfMaxDistance;
			
			/**
			 * Der Anteil an Winkeln, der �berpr�ft werden soll
			 */
			private static final double partOf360 = 0.3;
			
			/**
			 * Ein Konstruktor der nur den Anteil an der erlaubten Distanz speichert
			 * @param currentNumber
			 * @param maximumNumber
			 */
			public TrieOptimal(double currentNumber, double maximumNumber){
				this(currentNumber / maximumNumber);
			}
			
			/**
			 * Ein Konstruktor der nur den Anteil an der erlaubten Distanz speichert
			 * @param partOfMaxDistance Der Anteil an der Maximal erlaubten Distanz der getestet wird
			 */
			public TrieOptimal(double partOfMaxDistance){
				super();
				this.partOfMaxDistance = partOfMaxDistance;
			}
			
			@Override
			public void run(){
				//In diesem Fall bedeutet die Zahl der Versuche, die Genauigkeit mit der die Maximaldistanz durchprobiert wird
				//Die Zahl an Winkeln die mit der �bergebenen Distanz durchprobiert werden
				double degrees = 360 * partOf360;
				double kQuantifier = 1/partOf360;
				Trie[] tries = new TrieExact[(int) (degrees)];
				double distance = partOfMaxDistance * getMaxMoveDistance();
				for(int k = 0; k < degrees; k ++){
					double angle = Math.toRadians(k * kQuantifier);
					tries[k] = new TrieExact(minDistanceCircle, angle, distance);
					tries[k].start();
				}
				//Vergleiche die Minimaldistanzen und �berpr�fe ob eine gr��ere als bisher entstanden ist
				double minDistance = newMinDistance;
				SynchronizedStore bestStore = synchronizedStore;
				for(Trie s : tries){
					try {
						s.join();
						double newDist = computeMinimalDistance(s.getNewSynchronizedStore());
						if(newDist > minDistance){
							bestStore = s.getNewSynchronizedStore();
							minDistance = newDist;
						}
					} catch (InterruptedException e) {
						break;
					}
				}
				setSynchronizedStore(bestStore);
				}
			
				/**
				 * Der Anteil an der insgesamt erlaubten Distanz, die der Kreis zur�cklegen soll
				 */
				public double getPartOfMaxDistance(){
					return partOfMaxDistance;
				}
				
				/**
				 * Bestimmt die in diesem Fall wichtige Minimaldistanz oder Durchschnittsdistanz der Kreise
				 * @return
				 */
				private double computeMinimalDistance(SynchronizedStore s) {
					if(onlyMinDistance)
						return s.minimalDistance();
					else
						return s.averageDistance();
				}
			}
		
			/**
			 * Ein Versuch bei deim ein Kreis um genau bestimmte Werte bewegt wird
			 * @author Niels
			 *
			 */
			public class TrieExact extends Trie {
				
				/**
				 * Die Distanz die der Kreis zur�cklegen soll
				 */
				private double distance;
				
				/**
				 * Der Winkel, um den der Kreis bewegt werden soll
				 */
				private double angle;
				
				/**
				 * Der Kreis der bewegt werden soll
				 */
				private Circle movingCircle;
				
				/**
				 * Erzeugt einen Versuch, der den �bergebenen Kreis an genau die �bergebene Stelle verschiebt
				 * @param movingCircle Der zu bewegende Kreis
				 * @param angle Der Winkel zur neuen Position in Radien
				 * @param distance Die Distanz zur neuen Position
				 */
				public TrieExact (Circle movingCircle,  double angle, double distance ){
					super();
					this.movingCircle = movingCircle;
					this.angle = angle;
					this.distance = distance;
				}
				
				public void run(){
					moveInDir(movingCircle, angle, distance);
				}
				
				/**
				 * Die Distanz die der Kreis zur�cklegen soll
				 */
				public double getDistance(){
					return distance;
				}
				
				/**
				 * Der Winkel, um den der Kreis bewegt werden soll
				 */
				public double getAngle(){
					return angle;
				}
				
				/**
				 * Der Kreis der bewegt werden soll
				 */
				public Circle getMovingCircle(){
					return movingCircle;
				}
			}

		}
	
}
