package computation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * Ein auf der Fl�che zu verteilender Kreis
 * @author nielstron
 *
 */
public class Circle implements Cloneable {
	
	/**
	 * Der Mittelpunkt des Kreises
	 */
	private Vertex midpoint;
	
	/**
	 * Der Radius des Kreises
	 */
	private double radius;
	
	public Circle(Vertex m, double r){
		if(m == null) throw new NullPointerException();
		midpoint = m;
		radius = r;
	}

	public Circle(Point2D coordsNew, double radius2) {
		this(new Vertex(coordsNew), radius2);
	}

	public Vertex getMidpoint() {
		return midpoint;
	}

	public void setMidpoint(Vertex midpoint) {
		this.midpoint = midpoint;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	/**
	 * Gibt zur�ck ob dieser Kreis einen anderen Kreis schneidet
	 * @param c Der andere Kreis
	 * @return Schneiden sich die Kreise?
	 */
	public boolean cuts(Circle c){
		if(distance(c) > 0)
			return false;
		else return true;
	}
	
	/**
	 * Gibt zur�ck ob der �bergebene Punkt auf dem Kreis liegt
	 * @param p Der andere Punkt
	 * @return Liegt der Punkt auf dem Kreis?
	 */
	public boolean cuts(Point2D p){
		if(distance(p) > 0)
			return false;
		else return true;
	}
	
	/**
	 * Gibt zur�ck ob sich der �bergebene Knoten auf dem Kreis befindet
	 * @param v Der andere Knoten
	 * @return Liegt der Knoten auf dem Kreis?
	 */
	public boolean cuts(Vertex v){
		return cuts(v.getCoords());
	}
	
	/**
	 * Gibt zur�ck ob dieser Kreis die �bergebene Linie schneidet
	 * @param l Die Linie
	 * @return Schneidet die Linie diesen Kreis?
	 */
	public boolean cuts(Line2D l){
		if(l.ptSegDist(getMidpoint().getCoords()) < getRadius()){
			return true;
		}
		return false;
	}
	
	/**
	 * Gibt die Distanz des Randes dieses Kreises von einem anderen Knoten wieder
	 * @param v Der andere Knoten
	 * @return Die Distanz von dem anderen Knoten
	 */
	public double distance(Vertex v){
		return distance(v.getCoords());
	}
	
	/**
	 * Gibt die Distanz des Randes dieses Kreises zum Rand eines andern Kreises wieder
	 * @param c Der andere Kreis
	 * @return Die Distanz zum andern Kreis
	 */
	public double distance(Circle c){
		return distance(c.getMidpoint()) - c.getRadius();
	}
	
	/**
	 * Gibt die Distanz des Randes dieses Kreises von einem anderen Punkt wieder
	 * @param p Der entfernte Punkt
	 * @return Die Distanz zum Punkt
	 */
	public double distance (Point2D p){
		return midpoint.distance(p) - radius;
	}
	
	/**
	 * Verschiebt den Kreis an den �bergebenen Punkt
	 * @param p
	 */
	public void moveTo (Point2D p){
		midpoint.setCoords(p);
	}
	
	/**
	 * Gibt ein Rechteck zur�ck, welches etwa die Fl�che darstellt, die durch den Kreis abgedeckt wird<br>
	 * In diesem Fall wird ein Quadrat erzeugt, dass simpel die Seitenl�nge des Kreisdurchmessers hat
	 * und den Kreis vollst�ndig umschlie�t
	 * @return
	 */
	public Rectangle2D getBounds(){
		return new Rectangle2D.Double(
			getMidpoint().getCoords().getX() - getRadius(),
			getMidpoint().getCoords().getY() - getRadius(),
			2*getRadius(),
			2*getRadius()
		);
	}
	
	/**
	 * Gibt die Fl�che des Kreises einheitlos zur�ck
	 * @return
	 */
	public double getArea(){
		return getArea(radius);
	}
	
	/**
	 * Gibt die Fl�che eines Kreises mit dem Radius <code>radius</code> einheitlos zur�ck
	 * @param radius Der Radius des Kreises
	 * @return Die Fl�che eines Kreises mit Radius <code>radius</code>
	 */
	public static double getArea(double radius){
		return Math.PI * Math.pow(radius, 2);
	}
	
	public boolean equals(Object c){
		if(c.getClass().equals(this.getClass())){
			return this.equals((Circle) c);
		}
		else return super.equals(c);
	}
	
	public boolean equals(Circle c){
		return c.getMidpoint().equals(this.getMidpoint()) && c.getRadius() == this.getRadius();
	}
	
	public String toString(){
		return "Circle {" + (int) midpoint.getCoords().getX()+ ", " + (int) midpoint.getCoords().getY() + ", " +(int) radius + "}";
	}
	
	public Object clone(){
		return new Circle((Vertex) midpoint.clone(), Double.valueOf(radius));
	}
	
	public static Set<Circle> parseCircles (String s){
		Set<Circle> circles = new HashSet<Circle>();
		if(s != null && !s.isEmpty()){
			Vector<String> strings = new Vector<String>();
			//Teile den String dort wo keine Zahlen sind
			strings.addAll(Arrays.asList(s.split("\\D+")));
			//Filtere alle leeren Strings heraus
			Vector<String> removeStrings = new Vector<String>();
			for(String t : strings){
				if(!t.matches("\\d+"))
					removeStrings.add(t);
			}
			strings.removeAll(removeStrings);
			//Reduziere die Gr��e des Vektors auf eine durch drei teilbare Zahl (2 Koordinaten plus Radius)
			strings.setSize((strings.size() / 3) * 3);
			//Extrahiere aus dem Stringarray die enthaltenen Zahlen
			for(int i = 0; i <  strings.size(); i += 3){
				Integer[] specs = new Integer[3];
				for(int j = 0; j < specs.length; j++){
					specs[j] = (int) Double.parseDouble(strings.get(i+j));
				}
				circles.add(new Circle(new Point2D.Double(specs[0], specs[1]), specs[2]));
			}
		}
		return circles;
	}
}
