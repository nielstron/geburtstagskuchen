package computation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.Set;

/**
 * Eine Form die aus beliebig vielen Dreiecken besteht
 * @author nielstron
 *
 */
public class Shape {
	
	private Set<Triangle> triangles;
	
	public Shape(Set<Triangle> setTriangles){
		this.setTriangles(setTriangles);
	}
	
	public Shape(Triangle... triangles){
		this.triangles = new HashSet<Triangle>();
		for(Triangle triangle : triangles){
			this.triangles.add(triangle);
		}
	}

	public Set<Triangle> getTriangles() {
		return triangles;
	}

	public void setTriangles(Set<Triangle> triangles) {
		this.triangles = triangles;
	}

	/**
	 * F�gt ein Dreick der Form hinzu
	 * @param t
	 */
	public void addTriangle(Triangle t){
		triangles.add(t);
	}
	
	/**
	 * Entfernt ein Dreick aus der Form
	 * @param t
	 */
	public void removeTriangle(Triangle t){
		triangles.remove(t);
	}
	
	public void removeTriangles(Set<Triangle> t){
		triangles.removeAll(t);
	}
	
	/**
	 * Entfernt einen Knoten aus der Form indem es jedes Dreieck entfernt, welches diesen enth�lt
	 * @param v
	 */
	public void removeVertex(Vertex v){
		Set<Triangle> removeT = new HashSet<Triangle>();
		for(Triangle t : triangles){
			if(t.getPoints().contains(v.getCoords())) 
				removeT.add(t);
		}
		removeTriangles(removeT);
	}
	
	/**
	 * Gibt zur�ck, ob der �bergebene Vertex in dieser Form enthalten ist
	 * @param v
	 * @return
	 */
	public boolean contains(Vertex v){
		return contains(v.getCoords());
	}
	
	/**
	 * Gibt zur�ck, ob der �bergebene Punkt auf der Form liegt
	 * @param p
	 * @return
	 */
	public boolean contains(Point2D p){
		for(Triangle t : triangles){
			if(t.contains(p))
				return true;
		}
		return false;
	}
	
	/**
	 * Gibt zur�ck ob der Kreis in der Form liegt
	 * @param c Der Kreis, der �berpr�ft werden soll
	 * @return Liegt der Kreis vollst�ndig innerhalb der Form?
	 */
	public boolean contains(Circle c){
		//Ist der Kreis in einem der Dreiecke enthalten oder
		//Schneidet er mindestens ein Dreieck? 
		for(Triangle t : triangles){
			if(t.contains(c))
				return true;
			if(t.intersects(c)){
				//Wenn er mindestens ein Dreieck schneidet, schneidet er den Rand?
				for(Line2D l : getOutline()){
					//Schneidet er den Rand liegt er nicht vollst�ndig innerhalb...
					if(c.cuts(l))
						return false;
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gibt zur�ck ob ein Dreieck mit denselben Knoten bereits enthalten ist
	 * @param t Das zu �berpr�fende Dreieck
	 * @return Ist bereits ein Dreieck mit denselben Knoten in dieser Form enthalten?
	 */
	public boolean includes(Triangle t){
		for(Triangle s : triangles){
			if(t.equals(s))
				return true;
		}
		return false;
	}
	
	/**
	 * Gibt die Rahmenlinien der Form zur�ck<br>
	 * Dazu werden alle Linien der Form ausgegeben, die nur einmal insgesamt in allen Dreicken vorhanden sind
	 * @return Eine Menge, die die Au�enlinien der Form enth�lt
	 */
	public Set<Line2D> getOutline(){
		Set<Line2D> outlines = new HashSet<Line2D>();
		for(Triangle t : triangles){
			outlines.addAll(t.getOutline());
		}
		//W�hle die Linien aus, die nicht doppelt vorhanden sind
		Set<Line2D> notOutlines = new HashSet<Line2D>();
		for(Line2D l : outlines){
			if(!notOutlines.contains(l)){
				for(Line2D m : outlines){
					if(linesEqual(l, m) && !l.equals(m)){
						notOutlines.add(l);
						notOutlines.add(m);
						break;
					}
				}
			}
		}
		outlines.removeAll(notOutlines);
		return outlines;
	}
	
	/**
	 * Gibt den kleinsten Abstand zum Rand des Dreiecks zum Punkt wieder
	 * @param p Der Punkt, zu dem der Abstand gemessen werden soll
	 * @return Der kleinste Abstand zwischen Punkt und Rand
	 */
	public double distanceToOutline (Point2D p){
		double minDist = Double.POSITIVE_INFINITY;
		// �berpr�fe f�r jede Kante der Form die Entfernung zum Punkt 
		// und gebe die Kleinste zur�ck
		for(Line2D l : getOutline()){
			double dist = l.ptSegDist(p);
			if(dist < minDist){
				minDist = dist;
			}
		}
		return minDist;
	}
	
	/**
	 * Gibt den kleinsten Abstand vom Rand der Form zum Kreis wieder
	 * @param c
	 * @return Der kleinste Abstand zwischen Kreis und Rand
	 */
	public double distanceToOutline (Circle c){
		return distanceToOutline(c.getMidpoint().getCoords()) - c.getRadius();
	}
	
	/**
	 * Gibt die n�chste Linie des Randes zum Punkt wieder
	 * @param p
	 * @return Die n�chste Linie des Randes zum Punkt
	 */
	public Line2D nearestOutline (Point2D p){
		double minDist = Double.POSITIVE_INFINITY;
		Line2D nearest = null;
		// �berpr�fe f�r jede Kante der Form die Entferung zum Punkt 
		// und gebe die kleinste zur�ck
		for(Line2D l : getOutline()){
			double dist = l.ptSegDist(p);
			if(dist < minDist){
				minDist = dist;
				nearest = l;
			}
		}
		return nearest;
	}
	
	/**
	 * Gibt die n�chste Linie des Randes zum Punkt wieder
	 * @param p
	 * @return Die n�chste Linie des Randes zum Punkt
	 */
	public Line2D nearestOutline (Circle c){
		return nearestOutline(c.getMidpoint().getCoords());
	}
	
	/**
	 * Gibt alle Punkte zur�ck, die in den Dreiecken der Shape enthalten sind
	 * @return
	 */
	public Set<Point2D> getPoints(){
		Set<Point2D> returnSet = new HashSet<Point2D>();
		for(Triangle t : triangles)
			returnSet.addAll(t.getPoints());
		return returnSet;
	}
	
	/**
	 * Gibt die Fl�che der Form einheitlos zur�ck
	 * @return
	 */
	public double getArea(){
		double area = 0;
		for(Triangle t : triangles){
			area += t.getArea();
		}
		return area;
	}
	
	/**
	 * Gibt zur�ck, ob diese Form die Fl�che des Dreiecks schneidet
	 * @param t Ein Dreieck
	 * @return Schneidet die Form das Dreieck? (Ein Dreieck dieser Form gibt IMMER <code>true<code> zur�ck)
	 */
	public boolean intersects(Triangle t){
		for(Triangle s : triangles){
			if(s.intersects(t)) return true;
		}
		return false;
	}
	
	/**
	 * Gibt zur�ck, ob diese Form die Fl�che des anderen Dreiecks schneidet,
	 * au�er die sich schneidenden Seiten sind identisch
	 * oder die Seiten schneiden sich im Start- oder Endpunkt
	 * @param t Ein Dreieck
	 * @return Schneidet das Dreieck die Form ( au�er an Seiten die identisch sind)?
	 */
	public boolean intersectsWOEL(Triangle t){
		for(Triangle s : triangles){
			if(s.intersectsWOEL(t)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gibt zur�ck ob zwei �bergebene Linien den selben Start- und Endpunkt haben
	 * oder die Linie in entgegengesetzter Richtung dieselbe ist
	 * @param l Die erste zu vergleichende Linie
	 * @param m Die zweite zu vergleichende Linie
	 * @return Sind die Linien parallel und an der selben Position?
	 */
	public static boolean linesEqual (Line2D l, Line2D m){
		return 	( m.getP1().equals(l.getP1()) && m.getP2().equals(l.getP2()) ) ||
				( m.getP1().equals(l.getP2()) && m.getP2().equals(l.getP1()) );
	}
	
	/**
	 * Gibt zur�ck ob sich die Linien im Start- oder Endpunkt schneiden (End Or Start)
	 * @param l Die erste zu vergleichende Linie
	 * @param m Die zweite zu vergleichende Linie
	 * @return
	 */
	public static boolean linesCutEOS(Line2D l, Line2D m){
		return	( m.getP1().equals(l.getP1()) || m.getP1().equals(l.getP2()) ) ||
				( m.getP2().equals(l.getP1()) || m.getP2().equals(l.getP2()) );
	}
	
	/**
	 * Gibt eine Linie von der �bergebenen Linie zum �bergebenen Punkt zur�ck<br>
	 * Diese zieht sich entweder vom Start- , Mittel- oder Endpunkt der Linie zum Punkt,
	 * je nachdem, welcher der Punkte n�her ist.
	 * @param l Die Linie, von der die Linie starten soll
	 * @param p Der Punkt, zu dem die Linie f�hren soll
	 * @return eine Linie von der �bergebenen Linie zum �bergebenen Punkt.
	 *  Diese zieht sich entweder vom Start- , Mittel- oder Endpunkt der Linie zum Punkt,
	 */
	public static Line2D lineToPoint(Line2D l, Point2D p){
		//Erzeuge eine Start-, Mittel-, oder Endpunktlinie zum �bergebenen Punkt sind
		Line2D.Double middleConnector = new Line2D.Double(midpoint(l), p);
		Line2D.Double startConnector =  new Line2D.Double(l.getP1(), p);
		Line2D.Double endConnector =  new Line2D.Double(l.getP2(), p);
		Line2D[] connectors = {middleConnector, startConnector, endConnector};
		
		//�berpr�fe ob Start-, Mittel-, oder Endpunkt am n�chsten zum �bergebenen Punkt sind und �bergebe die k�rzeste Linie
		Line2D shortestConnector = middleConnector;
		for(Line2D c : connectors){
			if(Shape.segmentLength(c) < Shape.segmentLength(shortestConnector)){
				shortestConnector = c;
			}
		}
		return shortestConnector;
	}
	
	/**
	 * Gibt den Mittelpunkt einer Linie zur�ck
	 * @param l Die Linie
	 * @return Der Mittelpunkt der Linie
	 */
	public static Point2D midpoint(Line2D l){
		double x = (l.getX1() + l.getX2()) / 2;
		double y = (l.getY1() + l.getY2()) / 2;
		return new Point2D.Double(x,y);
	}
	
	/**
	 * Gibt die L�nge der �bergebenen Linie zur�ck
	 * @param l Die Linie
	 * @return Der Betrag der Linie
	 */
	public static double segmentLength(Line2D l){
		if(l == null) throw new IllegalArgumentException("Es muss eine Linie �bergeben werden.");
		return l.getP1().distance(l.getP2());
	}
	
	public String toString(){
		return "Shape " + triangles;
	}
	
}
