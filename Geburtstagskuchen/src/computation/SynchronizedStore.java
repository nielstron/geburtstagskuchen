package computation;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SynchronizedStore implements Cloneable{
	
	/**
	 * Die Kreise, die verteilt werden sollen
	 */
	private Set<Circle> circles = new HashSet<Circle>();
	
	/**
	 * Die Form, die gef�llt werden soll
	 */
	private Shape shape = new Shape();
	
	/**
	 * Knoten, die sich f�r ein neues Dreick gemerkt werden sollen
	 */
	private Set<Vertex> rememberedVertices = new HashSet<Vertex>(3);

	public Set<Circle> getCircles() {
		return circles;
	}

	public synchronized void setCircels(Set<Circle> candles) {
		this.circles = candles;
	}

	public Shape getShape() {
		return shape;
	}

	public synchronized void setShape(Shape shape) {
		this.shape = shape;
	}
	
	/**
	 * Merkt sich eine weitere Ecke des neues Dreiecks (maximal 3) und erzeugt ein neues Dreieck, falls 3 Ecken gemerkt wurden
	 * @param v Die neue Ecke die sich gemerkt werden soll
	 * @param vertexRadius Radius in dem ein Knoten vom geklickten Punkt entfernt befinden darf, um ausgew�hlt zu werden
	 * @return Wie viele Kanten wurden sich gemerkt?
	 */
	public synchronized int rememberVertex(Vertex v, int vertexRadius){
		Vertex newV = null;
		//Ist der Knoten noch nicht gemerkt?
		if(!rememberedVertices.contains(v) && !rememberedVertices.contains(cuttingVertex(v.getCoords(), vertexRadius))){
			//Wurde bereits ein Dreieck gesetzt?
			if(shape.getTriangles().isEmpty() ){
				//Nein  -> Akzeptiere jeden Knoten
					newV = v;
			}
			else{
				//Ja -> Merke sich einen bereits bekannten Knoten und als dritten Knoten einen bekannten oder neuen
				//Befindet sich der neue Punkt im Umkreis von vertexRadius Pixeln von einem bereits gesetzten?
				//Und stellt das neue Dreieck nicht ein bereits enthaltenes Dreieck dar?
				if(rememberedVertices.size() < 2){
					newV = cuttingVertex(v.getCoords(), vertexRadius);
				}
				//wurden sich bereits 2 Knoten gemerkt
				//dann akzeptiere einen neuen, als 3. Knoten eines neuen Dreiecks oder einen bekannten
				else if(rememberedVertices.size() == 2){
					Vertex tmp = cuttingVertex(v.getCoords(), vertexRadius);
					// Ein Testdreieck, um geforderte Bedingungen zu �berpr�fen
					Set<Vertex> testTriangleSet = new HashSet<Vertex>();
					testTriangleSet.addAll(rememberedVertices);
					if(tmp != null){
						//Befindet sich der neue Punkt im umkreis von vertexRadius Pixeln von einem bereits gesetzten?
						//Und stellt das neue Dreieck nicht ein bereits enthaltenes Dreieck dar oder schneidet die bisherige Form?
						testTriangleSet.add(tmp);
						if(!shape.includes(new Triangle(testTriangleSet)) && !shape.intersectsWOEL(new Triangle(testTriangleSet)))
							newV = tmp;
					}
					else{
						testTriangleSet.add(v);
						// Ansonsten:
						// Es soll ein neuer Knoten empfangen werden -> Akzeptiere jeden Knoten
						//... der nicht f�r ein Dreieck sorgen w�rde, dass die Form schneidet
						if(!shape.intersectsWOEL(new Triangle(testTriangleSet)) ){
							newV = v;
						}
					}
				}
			}
		}
		// Wurde auf diese Weise ein Knoten gefunden, so wird er hinzugef�gt
		if(newV != null){
			rememberedVertices.add(newV);
		}
		
		// Wurden sich bereits 3 Knoten gemerkt, erzeuge ein neues Dreieck aus den bekannten Knoten
		if(rememberedVertices.size() > 2){
			newTriangle(rememberedVertices);
			forgetVertices();
		}
		return rememberedVertices.size();
	}
	
	/**
	 * Alle Knoten, die sich zum Aufbau eines neuen Dreicks gemerkt wurden
	 * @return
	 */
	public Set<Vertex> getRememberedVertices(){
		return rememberedVertices;
	}
	
	/**
	 * Erzeugt ein neues Dreieck anhand der �bergebenen Knoten und f�gt es der Form hinzu
	 * @param vertices
	 */
	public synchronized void newTriangle(Collection<Vertex> vertices){
		shape.addTriangle(new Triangle(vertices));
	}
	
	
	/**
	 * L�scht alle gemerkten Knoten
	 */
	public synchronized void forgetVertices(){
		rememberedVertices = new HashSet<Vertex>(3);
	}
	
	/**
	 * F�gt einen neuen Kreis zur Menge der zu verteilenden Kreise hinzu, falls
	 * <ul>
	 * <li>Der Kreis keinen anderen Kreis schneidet und</li>
	 * <li>Der Kreis im Bereich der Form liegt</li>
	 * </ul>
	 * @param coords Die Koordinaten des neuen Kreises
	 * @param radius Der Radius des neuen Kreises
	 * @return Wurde der Kreis erfolgreich hinzugef�gt?
	 */
	public synchronized boolean addCircle(Point2D coords, double radius){
		return addCircle(new Circle(new Vertex(coords), radius));
	}
	
	/**
	 * F�gt einen neuen Kreis zur Menge der zu verteilenden Kreise hinzu, falls
	 * <ul>
	 * <li>Der Kreis keinen anderen Kreis schneidet und</li>
	 * <li>Der Kreis im Bereich der Form liegt</li>
	 * </ul>
	 * @param coords Die Koordinaten des neuen Kreises
	 * @param radius Der Radius des neuen Kreises
	 * @return Wurde der Kreis erfolgreich hinzugef�gt?
	 */
	public synchronized boolean addCircle(Circle d){
		if(validCircle(d))
			return circles.add(d);
		return false;
	}
	
	/**
	 * Verschiebt einen Kreis, der die �bergegebenen Koordinaten schneidet an die neuen Koordinaten
	 * @param coordsOld
	 * @param coordsNew
	 */
	public synchronized boolean moveCircle(Point2D coordsOld, Point2D coordsNew){
		Circle c = cuttingCircle(coordsOld);
		if(c != null){
			Circle newC = new Circle(coordsNew, c.getRadius());
			if(shape.contains(newC)){
				boolean proceed = true;
				for(Circle d : getCircles()){
					if(d.cuts(newC) && !d.equals(c)){
						proceed = false;
						break;
					}
				}
				if(proceed)
					c.moveTo(coordsNew);
				return proceed;
			}
		}
		return false;
	}
	
	
	/**
	 * Gibt zur�ck, ob der �bergebene Kreis gespeichert werden darf
	 * @param c Der hinzuzuf�gende Kreis
	 * @return Wurde der Kreis akzeptiert?
	 */
	public boolean validCircle(Circle c){
		if(cuttingCircle(c) == null && shape.contains(c))
			return true;
		return false;
	}
	
	/**
	 * Gibt zur�ck, ob der �bergebene Kreis gespeichert werden darf
	 * @param p Der Mittelpunkt des Kreises
	 * @param radius Der Radius des Kreises
	 * @return
	 */
	public boolean validCircle(Point2D p, double radius){
		return validCircle(new Circle(new Vertex(p), radius));
	}
	
	/**
	 * Entfernt den Kreis, der die �bergebenen Koordinaten schneidet
	 * @param coords Koordinaten innerhalb des zu l�schenden Kreises
	 */
	public synchronized void removeCircle(Point2D coords){
		Circle c = cuttingCircle(coords);
		if(c != null)
			circles.remove(c);
	}
	
	/**
	 * Entfernt den Knoten, der im Umkreis von r um die Koordinaten liegt
	 * @param coords Koordinaten in der N�he des zu l�schenden Knotens
	 */
	public synchronized void removeVertex(Point2D coords, double r){
		Vertex v = cuttingVertex(coords, r);
		if(v != null)
			shape.removeVertex(v);
	}
	
	/**
	 * Gibt einen Kreis der Menge der Kreise zur�ck, falls dieser den �bergebenen Punkt beinhaltet<br>
	 * Ansonsten <code>null</node>
	 * @param coordsOld
	 * @return
	 */
	public Circle cuttingCircle (Point2D coordsOld){
		for(Circle c : getCircles()){
			if(c.cuts(coordsOld)) 
				return c;
		}
		return null;
	}
	
	/**
	 * Gibt einen Kreis der Menge der Kreise zur�ck, falls dieser den �bergebenen Kreis schneidet<br>
	 * Ansonsten <code>null</code>
	 * @param c
	 * @return
	 */
	public Circle cuttingCircle (Circle c){
		for(Circle d : getCircles()){
			if(d.cuts(c)) 
				return d;
		}
		return null;
	}
	
	/**
	 * Gibt einen Vertex aus der Form zur�ck, falls Punkt p im Umkreis r von ihm liegt
	 * @param point2d
	 * @param r
	 * @return
	 */
	public Vertex cuttingVertex (Point2D point2d, double r){
		Circle c = new Circle(new Vertex(point2d), r);
		for(Vertex v : rememberedVertices){
			if(c.cuts(v))
				return v;
		}
		for(Triangle t : shape.getTriangles()){
			for( Vertex v : t.getVertices()){
				if(c.cuts(v))
					return v;
			}
		}
		return null;
	}

	/**
	 * Kopiert alle Werte vom �bergebenen {@link SynchronizedStore} in den eigenen
	 * @param ss
	 * @throws CloneNotSupportedException 
	 */
	public synchronized void copyFrom(SynchronizedStore ss) {
		circles = new HashSet<Circle>();
		for(Circle c : ss.getCircles()){
			circles.add((Circle) c.clone());
		}
		shape = new Shape();
		Set<Triangle> t = new HashSet<Triangle>();
		t.addAll(ss.getShape().getTriangles());
		shape.setTriangles(t);
	}
	
	/**
	 * Gibt den kleinsten Abstand des �bergebenen Kreises zu einem anderen Kreis oder dem Rand der Form zur�ck
	 * @param c Ein Kreis, dessen Minimalabstand bestimmt werden soll
	 * @return Der kleinste Abstand dieses Kreises zu einem anderen Kreis oder dem Rand der Form
	 */
	public synchronized double minimalDistance(Circle c){
		//Der aktuelle Minimalabstand
		double minDist = Double.POSITIVE_INFINITY;
		//�berpr�fe ob der Abstand zu einem anderen Kreis kleiner ist
		//als das bisherige Minimum
		for(Circle d : circles){
			double newDist = c.distance(d);
			if( newDist < minDist && !d.equals(c)){
				minDist = newDist;
			}
		}
		//�berpr�fe ob der Abstand zu einem Rand kleiner ist als das bisherige Minimum
		{
			double newDist = getShape().distanceToOutline(c);
			if( newDist < minDist){
				minDist = newDist;
			}
		}
		return minDist;
	}
		
	
	/**
	 * Gibt den kleinsten Abstand zwischen zwei Punkten oder einem Punkt und dem Rand der Form zur�ck
	 * der in der aktuellen Form enthaltenen Punkte und Form zur�ck
	 * @return
	 */
	public synchronized double minimalDistance(){
		//Der aktuelle Minimalabstand
		double minDist = Double.POSITIVE_INFINITY;
		//�berpr�fe f�r jeden Kreis
		for(Circle c : circles){
			//Ob dessen kleinster Abstand kleiner ist als das bisherige Minimum
			double newDist = minimalDistance(c);
			if(newDist < minDist){
				minDist = newDist;
			}
		}
		return minDist;
	}
	
	/**
	 * Gibt den Durchschnitt aller Minimaldistanzen aller Kreise in der Form zur�ck
	 * @return Der Durchschnitt aller Minimaldistanzen aller Kreise in der Form
	 */
	public synchronized double averageDistance(){
		//Die Summe aller Distanzen
		double sum = 0;
		//Iteriere �ber jeden Kreis
		for(Circle c : circles){
			//F�ge seine minimaldistanz zur summe aller minimaldistanzen hinzu
			sum += minimalDistance(c);
		}
		//Gebe die summe aller minimaldistanzen geteilt durch die zahl aller kreise zur�ck
		return (sum / circles.size());
	}
	
	/**
	 * Gibt die k�rzeste Linie zur�ck, die einen Punkt mit einem anderen Punkt oder dem Rand verbindet
	 * Dabei ist Punkt 2 der Kreis zu dem der k�rzeste Abstand besteht
	 * und Punkt 1 entweder der Rand oder der Kreis von dem der Abstand besteht
	 * @return
	 */
	public synchronized Line2D shortestConnectingLine(){
		//Der aktuelle Minimalabstand
		double minDist = Double.POSITIVE_INFINITY;
		Line2D shortestLine = null;
		//�berpr�fe f�r jeden Kreis
		for(Circle c : circles){
			//�berpr�fe ob der Abstand zu einem anderen Kreis kleiner ist
			//als das bisherige Minimum
			for(Circle d : circles){
				double newDist = c.distance(d);
				if( newDist < minDist && !d.equals(c)){
					minDist = newDist;
					shortestLine = new Line2D.Double(d.getMidpoint().getCoords(), c.getMidpoint().getCoords());
				}
			}
			//�berpr�fe ob der Abstand zum Rand kleiner ist als das bisherige Minimum
			{
				double newDist = getShape().distanceToOutline(c);
				if( newDist < minDist){
					minDist = newDist;
					shortestLine = Shape.lineToPoint(getShape().nearestOutline(c), c.getMidpoint().getCoords());
				}
			}
		}
		return shortestLine;
	}
	
	/**
	 * Gibt den optimalen Abstand der Kreismittelpunkte zur�ck, ohne R�cksicht auf deren Radius
	 * @return Der Abstand der Kreismittelpunkte f�r die Optimalverteilung
	 */
	public synchronized double optimalDistance(){
		double radiusSum = 0;
		double radiusSumSquare = 0;
		int n = circles.size();
		for(Circle c : circles){
			radiusSum += c.getRadius();
			radiusSumSquare += Math.pow(c.getRadius(), 2);
		}
		double discriminant = Math.PI * (Math.PI * Math.pow(radiusSum, 2) - Math.PI * n * radiusSumSquare + shape.getArea() * n);
		//System.out.println(discriminant);
		double optDist = (-1 * Math.PI * radiusSum + (Math.sqrt(discriminant)) ) / ((Math.PI * n)/2);
		//System.out.println(optDist);
		if(optDist < 0){
			optDist = (-1 * Math.PI * radiusSum - (Math.sqrt(discriminant)) ) / ((Math.PI * n)/2);
		}
		return optDist;
		// Die alte Methode, ohne Ber�cksichtigung der Kreisradien
		//return Math.sqrt((4 * shape.getArea()) / (circles.size() * Math.PI));
	}
	
	/**
	 * Berechnet die Gleichm��igkeit der verteilten Kreise auf der Fl�che
	 * @return
	 */
	public synchronized double computeRegularity(){
		if(shape == null || shape.getTriangles().isEmpty() || circles.isEmpty()) return 0;
		double regularity = 0;
			//Der kleinste Abstand zwischen Punkt und Punkt oder Punkt und Rand
			double minDist = minimalDistance();
			//Die Fl�che, die von allen Kreisen zusammen mit der halben minimaldistanz eingenommen wird
			double circleArea = 0;
			for(Circle c : circles){
				circleArea += Circle.getArea(c.getRadius() + 0.5 * minDist);
			}
			//Die Fl�che, die von der Form eingenommen wird (Gesamtfl�che)
			double shapeArea = shape.getArea();
		// Die Gleichm��igkeit wird durch den Anteil der Kreisfl�che an der Gesamtfl�che dargestellt;
		regularity = circleArea / shapeArea;
		return regularity;
	}
	
	public Object clone() {
		SynchronizedStore  sync = new SynchronizedStore();
		sync.copyFrom(this);
		return sync;
	}
}
