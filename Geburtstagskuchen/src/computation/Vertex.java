package computation;

import java.awt.geom.Point2D;

/**
 * Ein Knoten, der in diesem Fall die Ecke eines Dreiecks darstellt
 * @author nielstron
 *
 */
public class Vertex implements Cloneable {
	
	private Point2D coords;
	
	public Vertex(double x, double y){
		setCoords(new Point2D.Double(x, y));
	}
	
	public Vertex(Point2D setCoords){
		if(setCoords == null) throw new NullPointerException("Die uebergebenen Koordinaten d�rfen nicht null sein");
		setCoords(setCoords);
	}
	
	public Point2D getCoords() {
		return coords;
	}

	public void setCoords(Point2D coords) {
		this.coords = coords;
	}
	
	public void setCoords(double x, double y) {
		setCoords(new Point2D.Double(x, y));
	}
	
	/**
	 * Gibt an, ob das �bergebene Object diesem entspricht<bR>
	 * Haben beide Objekte die Klasse Vertex, so wird equals(Vertex v) aufgerufen
	 * @param v
	 * @return
	 */
	public boolean equals(Object v){
		if(v.getClass().equals(this.getClass()))
			return this.equals((Vertex) v);
		else return super.equals(v);
	}
	
	/**
	 * Gibt die Distanz dieses Knotens zu einem Punkt zur�ck
	 * @param point2d Der entfernte Punkt
	 * @return Die Distanz der Knoten
	 */
	public double distance(Point2D point2d){
		double xDistance = point2d.getX() - coords.getX();
		double yDistance = point2d.getY() - coords.getY();
		return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
	}

	/**
	 * Gibt die Distanz dieses Knotens zum anderen Knoten zur�ck
	 * @param v Der entfernte Knoten
	 * @return Die Distanz der Knoten
	 */
	public double distance(Vertex v){
		return distance(v.getCoords());
	}
	
	/**
	 * Gibt zur�ck ob dieser Knoten innerhalb eines Kreises liegt
	 * @param c Der Kreis
	 * @return Liegt der Knoten innerhalb des Kreises?
	 */
	public boolean liesInside(Circle c){
		if(c.distance(coords) > c.getRadius())
			return false;
		else return true;
	}
	
	/**
	 * Gibt zur�ck, ob dieser Knoten dieselben Koordinaten hat, wie der �bergebene Knoten
	 * @param v
	 * @return
	 */
	public boolean equals(Vertex v){
		return coords.equals(((Vertex) v).getCoords());
	}
	
	/**
	 * Bewegt den Knoten an die �bergebene Position
	 * @param x X-Koordinate der neuen Position
	 * @param y Y-Koordinate der neuen Position
	 */
	public void moveTo(double x, double y){
		setCoords(x,y);
	}
	
	/**
	 * Bewegt den Knoten um die �bergebenen Kooridnaten in x- und y-Richtung
	 * @param x X-Koordinate um die verschoben werden soll
	 * @param y Y-Koordinate um die verschoben werden soll
	 */
	public void moveBy(double x, double y){
		setCoords(coords.getX() + x, coords.getY() + y);
	}
	
	/**
	 * Bewegt den Knoten im �bergebenen Winkel um die �bergebene Entferung
	 * @param angle Winkel in Bogenma�, in dem der Punkt verschoben werden soll
	 * @param distance Strecke, um die der Punkt verschoben werden soll
	 */
	public void moveInDir(double angle, double distance){
		setCoords(pointInDir(angle, distance));
	}
	
	/**
	 * Bestimmt den Winkel in Bogenma� in dem der Punkt p zu diesem Knoten liegt
	 * @param p Der Punkt zu dem der Winkel bestimmt werden soll
	 * @return Der Winkel der Punkte zueinander in Bogenma�
	 */
	public double computeAngle(Point2D p){
		return Math.atan( this.getCoords().getY() - p.getY() / this.getCoords().getX() - p.getX() );
	}
	
	/**
	 * Bestimmt den Winkel in Bogenma�, in dem der Knoten v zu diesem Knoten liegt
	 * @param v Der Knoten zu dem der Winkel bestimmt werden soll
	 * @return Der Winkel der Knoten zueinander in Bogenma�
	 */
	public double computeAngle(Vertex v){
		return computeAngle(v.getCoords());
	}
	
	/**
	 * Gibt den Punkt zur�ck, der in dem �bergebenen Winkel in der �bergebenen Distanz zu diesem Punkt liegt
	 * @param angle Winkel in Bogenma�, in dem der neue Punkt zum aktuellen Punkt liegt
	 * @param distance Distanz des neuen Punktes zum aktuellen Punkt
	 * @return Ein Punkt dessen Distanz und Winkel zum aktuellen Punkt sehr genau mit den �bergebenen Werten �bereinstimmen
	 */
	public Point2D pointInDir(double angle, double distance){
		double x = Math.cos(angle) * distance;
		double y = Math.sin(angle) * distance;
		return new Point2D.Double(x + coords.getX() , y + coords.getY());
	}
	
	public Object clone() {
		return new Vertex((Point2D) coords.clone());				
	}
	
	
	public String toString(){
		return "Vertex [" + coords.getX() + ", " + coords.getY() + "]";
	}
	
}
