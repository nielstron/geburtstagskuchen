package gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;

import computation.Circle;
import computation.DrawDistributionThread;
import computation.EvAlgThread;
import computation.FileInput;
import computation.Main;
import computation.Triangle;
import data.DataReference;
import computation.ThreadMonitor;
import gui.GuiCanvas.CirclesCanvas;
import gui.GuiCanvas.DistributionShapesCanvas;
import gui.GuiCanvas.ShapeCanvas;
import jdk.nashorn.internal.scripts.JD;

import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;

import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

import java.util.HashSet;
import java.util.Set;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.JCheckBox;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JProgressBar;
import java.awt.FlowLayout;
import java.awt.CardLayout;
import javax.swing.SwingConstants;

public class AppWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4096536859831096673L;
	
	
	private static Dimension panelSize = new Dimension(300,300);
	
	private JLayeredPane layeredPane;
	private CirclesCanvas circlePanel;
	private ShapeCanvas shapePanel;
	private JTabbedPane tabbedPane;
	private JButton readShapeButton;
	private JButton setCirclesButton;
	private JButton spreadCirclesButton;
	private JLabel dimensionLabel;
	private JPanel shapeBuildPane;
	private JPanel contentPane;
	private JTextField computeRegularityField;
	private ReadShapeDialog readShapeWindow;
	private ReadCirclesDialog readCirclesWindow;
	private JSpinner dimensionHeightField;
	private JSpinner dimensionWidthField;
	private JPanel background;
	private AppWindow self;
	private JButton moveCirclesRandomlyButton;
	private DistributionShapesCanvas distributionPanel;
	private JCheckBox showDistributionPanelCheckbox;
	private JSpinner eaTriesSpinner;
	private JSpinner eaStepsSpinner;
	private JLabel eaStepsLabel;
	private JLabel eaTriesLabel;
	private JPanel distributionPane;
	private JPanel spreadSettings;
	private JLabel distributionMethodLabel;
	private JPanel distributionMethods;
	private JRadioButton distributionMethodAllCircles;
	private JRadioButton distributionMethodOneCircle;
	private JRadioButton distributionMethodBoth;
	private ThreadMonitor algMonitor;
	private EvAlgThread evAlg;
	private final ButtonGroup distributionMethodGroup = new ButtonGroup();
	private JButton distributionStopButton;
	private JCheckBox silentMutationCheckbox;
	private JCheckBox onlyMinDistanceCheckbox;
	private JRadioButton distributionMethodOptimal;
	private JCheckBox showMaxMoveDistanceCheckbox;
	private JProgressBar progressBar;
	private JLayeredPane distributionStartPane;
	private JLabel currentRegularityLabel;
	private JPanel currentRegularityPanel;


	/**
	 * Create the frame.
	 */
	public AppWindow() {
		self = this;
		setTitle("Geburtstagskuchen");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AppWindow.class.getResource("/icon.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 506, 335);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		//Setze als Anfangsform und Kreise die Werte aus der Aufgabenstellung
		try {
			URL url = DataReference.class.getResource("herzform.txt");
			File heartFile = new File(url.toURI());
			String heartString = FileInput.fileToString(heartFile);
			Set<Triangle> triangles = Triangle.parseTriangles(heartString);
			Main.getSynchronizedStore().getShape().getTriangles().addAll(triangles);
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
		}
		try{
			URL url = DataReference.class.getResource("kreispositionen.txt");
			File circleFile = new File(url.toURI());
			String circleString = FileInput.fileToString(circleFile);
			Set<Circle> circles = Circle.parseCircles(circleString);
			Main.getSynchronizedStore().setCircels(circles);
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
		}
		
		layeredPane = new JLayeredPane();
		layeredPane.setBackground(Color.WHITE);

		contentPane.add(layeredPane, BorderLayout.EAST);
		algMonitor = new ThreadMonitor((Thread) null, (JComponent[]) null);
		layeredPane.setLayout(null);
		
		JLabel algorithmLabel = new JLabel("Algorithmus");
		algorithmLabel.setBounds(0, 0, 56, 14);
		algorithmLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		layeredPane.add(algorithmLabel);
		
		JLabel activeLabel = new JLabel("inaktiv");
		activeLabel.setBounds(59, 0, 64, 14);
		activeLabel.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName().equals("activityIndicator")){
					if((boolean) arg0.getNewValue()){
						activeLabel.setForeground(new Color(255, 0, 0));
						activeLabel.setText("aktiv");
					}
					else{
						activeLabel.setForeground(new Color(0, 255, 0));
						activeLabel.setText("inaktiv");
					}
				}
			}
		});
		activeLabel.setForeground(Color.GREEN);
		activeLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		layeredPane.add(activeLabel);
		
		distributionPanel = new GuiCanvas.DistributionShapesCanvas(){
			private static final long serialVersionUID = -3766245268049993246L;

			@Override
			public void paint(Graphics g){
				updateRegularityValue();
				super.paint(g);
			}
		};
		distributionPanel.setBounds(0, 0, 0, 0);
		distributionPanel.setBackground(new Color(0,0,0,0));
		layeredPane.add(distributionPanel);
		
		circlePanel= new GuiCanvas.CirclesCanvas();
		circlePanel.setBounds(0, 0, 0, 0);
		layeredPane.add(circlePanel);
		
		shapePanel = new GuiCanvas.ShapeCanvas();
		shapePanel.setBounds(0, 0, 0, 0);
		layeredPane.add(shapePanel);
		
		background = new JPanel();
		background.setBounds(0, 0, 0, 0);
		background.setBackground(Color.WHITE);
		layeredPane.add(background);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.WEST);
		
		shapeBuildPane = new JPanel();
		tabbedPane.addTab("Formaufbau", null, shapeBuildPane, null);
		tabbedPane.setEnabledAt(0, true);
		shapeBuildPane.setLayout(new MigLayout("", "[99px,grow]", "[23px][23px][][][][][][][][][][]"));
		
		distributionPane = new JPanel();
		tabbedPane.addTab("Verteilung", null, distributionPane, null);
		distributionPane.setLayout(new MigLayout("", "[99px,grow]", "[23px][23px][][][][][][][][]"));
		
		readShapeWindow = new ReadShapeDialog(this);
		readShapeButton = new JButton("Form einlesen");
		readShapeButton.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals("activityIndicator")){
					if((boolean) evt.getNewValue()){
						readShapeButton.setEnabled(false);
					}
					else{
						readShapeButton.setEnabled(true);
					}
				}
			}
		});
		readShapeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//L�sche die Kreise, falls welche gesetzt wurden => Warne den Nutzer
				int n = 0;
				if(!Main.getSynchronizedStore().getCircles().isEmpty()){
					n = JOptionPane.showConfirmDialog(
					    self,
					    "Bereits gesetzte Kreise werden gel�scht. Sind sie sicher?",
					    "Kreisl�schung",
					    JOptionPane.YES_NO_OPTION);
				}
				if(n == 0){
					Main.getSynchronizedStore().setCircels(new HashSet<Circle>());
					readShapeWindow.repaint();
					readShapeWindow.setVisible(true);
				}
			}
		});
		
		dimensionLabel = new JLabel("Dimensionen");
		dimensionLabel.setToolTipText("Die Dimensionen der Leinwand, auf der gezeichnet wird");
		shapeBuildPane.add(dimensionLabel, "flowx,cell 0 0");
		readShapeButton.setToolTipText("\u00D6ffnet ein Fenster um die Form zu bearbeiten");
		shapeBuildPane.add(readShapeButton, "cell 0 1,growx,aligny top");
		
		readCirclesWindow = new ReadCirclesDialog(this);
		setCirclesButton = new JButton("Kreise / Kerzen setzen");
		setCirclesButton.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName().equals("activityIndicator")){
					if((boolean) arg0.getNewValue()){
						setCirclesButton.setEnabled(false);
					}
					else{
						setCirclesButton.setEnabled(true);
					}
				}
			}
		});
		setCirclesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				readCirclesWindow.repaint();
				readCirclesWindow.setVisible(true);
			}
		});
		setCirclesButton.setToolTipText("\u00D6ffnet ein Fenster um Kreise in der Form zu verteilen");
		shapeBuildPane.add(setCirclesButton, "cell 0 2,growx,aligny top");
		
		moveCirclesRandomlyButton = new JButton("Bewege Kreise zuf\u00E4llig");
		moveCirclesRandomlyButton.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName().equals("activityIndicator")){
					if((boolean) arg0.getNewValue()){
						moveCirclesRandomlyButton.setEnabled(false);
					}
					else{
						moveCirclesRandomlyButton.setEnabled(true);
					}
				}
			}
		});
		moveCirclesRandomlyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(Main.getSynchronizedStore().getCircles().isEmpty()) return;
				EvAlgThread.TrieRow row = new EvAlgThread.TrieRow(0);
				EvAlgThread.TrieRow.TrieAll trie = row.new TrieAll();
				trie.start();
				try{
					trie.join();
				}
				catch(InterruptedException ie){
					//TODO etwas machen
					System.out.println(ie.getStackTrace());
				}
				Main.getSynchronizedStore().copyFrom(trie.getNewSynchronizedStore());
				circlePanel.repaint();
			}
		});
		distributionPane.add(moveCirclesRandomlyButton, "flowx,cell 0 0,growx");
		
		spreadSettings = new JPanel();
		spreadSettings.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals("activityIndicator")){
					if((boolean) evt.getNewValue()){
						eaTriesSpinner.setEnabled(false);
						eaStepsSpinner.setEnabled(false);
					}
					else{
						eaTriesSpinner.setEnabled(true);
						eaStepsSpinner.setEnabled(true);
					}
				}
			}
		});
		distributionPane.add(spreadSettings, "cell 0 2,grow");
		spreadSettings.setLayout(new MigLayout("", "[99px,grow]", "[]"));
		
		eaStepsSpinner = new JSpinner();
		eaStepsSpinner.setToolTipText("Die Anzahl der Versuchsreihen die gestartet werden sollen. -1 bedeutet unendlich.");
		spreadSettings.add(eaStepsSpinner, "flowx,cell 0 0,growx");
		eaStepsSpinner.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(-1), null, new Integer(1)));
		
		eaStepsLabel = new JLabel("Schritte \u00E0 ");
		spreadSettings.add(eaStepsLabel, "cell 0 0");
		
		eaTriesSpinner = new JSpinner();
		eaTriesSpinner.setToolTipText("Die Anzahl an Versuchen pro Versuchsreihe");
		spreadSettings.add(eaTriesSpinner, "cell 0 0,growx");
		eaTriesSpinner.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		
		eaTriesLabel = new JLabel("Versuche");
		spreadSettings.add(eaTriesLabel, "cell 0 0");
		
		distributionMethods = new JPanel();
		distributionMethods.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals("activityIndicator")){
					if((boolean) evt.getNewValue()){
						distributionMethodAllCircles.setEnabled(false);
						distributionMethodOneCircle.setEnabled(false);
						distributionMethodBoth.setEnabled(false);
						distributionMethodOptimal.setEnabled(false);
					}
					else{
						distributionMethodAllCircles.setEnabled(true);
						distributionMethodOneCircle.setEnabled(true);
						distributionMethodBoth.setEnabled(true);
						distributionMethodOptimal.setEnabled(true);
					}
				}
			}
		});
		distributionPane.add(distributionMethods, "cell 0 3,grow");
		distributionMethods.setLayout(new MigLayout("", "[99px]", "[14px]"));
		
		distributionMethodLabel = new JLabel("Verteilungsmethode:    Bewege...");
		distributionMethods.add(distributionMethodLabel, "flowy,cell 0 0,growx,aligny top");
		
		distributionMethodAllCircles = new JRadioButton("alle Kreise simultan");
		distributionMethodGroup.add(distributionMethodAllCircles);
		distributionMethods.add(distributionMethodAllCircles, "cell 0 0,growx");
		
		distributionMethodOneCircle = new JRadioButton("nur den Kreis mit geringstem Abstand");
		distributionMethodGroup.add(distributionMethodOneCircle);
		distributionMethods.add(distributionMethodOneCircle, "cell 0 0");
		
		distributionMethodBoth = new JRadioButton("die obigen Methoden gemischt");
		distributionMethodBoth.setSelected(true);
		distributionMethodGroup.add(distributionMethodBoth);
		distributionMethods.add(distributionMethodBoth, "cell 0 0");
		
		distributionMethodOptimal = new JRadioButton("den Kreis mit geringstem Abstand optimal");
		distributionMethodGroup.add(distributionMethodOptimal);
		distributionMethodOptimal.setToolTipText("Bewegt den Kreis mit dem geringsten Abstand zu einem anderen Kreis oder dem Rand in m\u00F6glichst viele Richtung (nicht zuf\u00E4llig) um das m\u00F6glichst optimale Ergebnis zu erzielen");
		distributionMethods.add(distributionMethodOptimal, "cell 0 0");
		
		silentMutationCheckbox = new JCheckBox("Erlaube \"Stille Mutation\"");
		silentMutationCheckbox.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals("activityIndicator")){
					if((boolean) evt.getNewValue()){
						silentMutationCheckbox.setEnabled(false);
					}
					else{
						silentMutationCheckbox.setEnabled(true);
					}
				}
			}
		});
		silentMutationCheckbox.setSelected(true);
		silentMutationCheckbox.setToolTipText("Erlaube Ver\u00E4nderungen, auch wenn direkt keine gr\u00F6\u00DFere Gleichm\u00E4\u00DFigkeit erreicht wird");
		distributionPane.add(silentMutationCheckbox, "flowx,cell 0 4,alignx left");
		
		onlyMinDistanceCheckbox = new JCheckBox("Beachte nur den Minimalabstand");
		onlyMinDistanceCheckbox.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals("activityIndicator")){
					if((boolean) evt.getNewValue()){
						onlyMinDistanceCheckbox.setEnabled(false);
					}
					else{
						onlyMinDistanceCheckbox.setEnabled(true);
					}
				}
			}
		});
		onlyMinDistanceCheckbox.setSelected(true);
		onlyMinDistanceCheckbox.setToolTipText("Ein Versuch ist nur dann besser, wenn der neue Minimalabstand kleiner ist als zuvor. Alternativ kann der Durchschnittsabstand betrachtet werden.");
		distributionPane.add(onlyMinDistanceCheckbox, "cell 0 5");
		
		distributionStartPane = new JLayeredPane();
		distributionPane.add(distributionStartPane, "flowx,cell 0 1,grow");
		distributionStartPane.setLayout(new CardLayout(0, 0));
		
		spreadCirclesButton = new JButton("Verteile die Kreise gleichm\u00E4\u00DFig");
		spreadCirclesButton.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName().equals("activityIndicator")){
					if((boolean) arg0.getNewValue()){
						spreadCirclesButton.setEnabled(false);
						((CardLayout) distributionStartPane.getLayout()).last(distributionStartPane);
					}
					else{
						spreadCirclesButton.setEnabled(true);
						((CardLayout) distributionStartPane.getLayout()).first(distributionStartPane);
					}
				}
			}
		});
		spreadCirclesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				evAlg = new EvAlgThread(
						Main.getSynchronizedStore(),
						(int) eaStepsSpinner.getValue(),
						(int) eaTriesSpinner.getValue(),
						getDistributionMethod(),
						silentMutationCheckbox.isSelected(),
						onlyMinDistanceCheckbox.isSelected()
				);
				algMonitor.setThread(evAlg);
				JComponent[] components = {
						distributionPanel,
						circlePanel
				};
				@SuppressWarnings("unused")
				DrawDistributionThread draw = new DrawDistributionThread(evAlg, components);
			}
		});
		distributionStartPane.setLayer(spreadCirclesButton, 1);
		distributionStartPane.add(spreadCirclesButton, "name_329851379131969");
		
		progressBar = new JProgressBar();
		distributionStartPane.add(progressBar, "name_329851400981954");
		
		distributionStopButton = new JButton("Stopp");
		distributionStopButton.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName().equals("activityIndicator")){
					if((boolean) arg0.getNewValue()){
						distributionStopButton.setEnabled(true);
					}
					else{
						distributionStopButton.setEnabled(false);
					}
				}
			}
		});
		distributionStopButton.setEnabled(false);
		distributionStopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				evAlg.interrupt();
			}
		});
		
		distributionPane.add(distributionStopButton, "cell 0 1,alignx right,growy");
		
		currentRegularityPanel = new JPanel();
		distributionPane.add(currentRegularityPanel, "cell 0 7,alignx center,aligny center");
		currentRegularityPanel.setBackground(new Color(0,0,0,0));
		currentRegularityPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		currentRegularityLabel = new JLabel("Aktuelle Gleichm\u00E4\u00DFigkeit");
		currentRegularityLabel.setHorizontalAlignment(SwingConstants.CENTER);
		currentRegularityPanel.add(currentRegularityLabel);
		
		computeRegularityField = new JTextField();
		distributionPane.add(computeRegularityField, "cell 0 8,grow");
		computeRegularityField.setBackground(Color.WHITE);
		computeRegularityField.setEditable(false);
		computeRegularityField.setToolTipText("Berechnete Gleichm\u00E4\u00DFigkeit der verteilten Kreise");
		computeRegularityField.setColumns(10);
		
		ChangeListener dimensionListener = new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				readPanelSize();
				applyPanelSize();
			}
		};
		
		dimensionHeightField = new JSpinner();
		dimensionHeightField.setModel(new SpinnerNumberModel(new Integer(300), new Integer(0), null, new Integer(1)));
		dimensionHeightField.addChangeListener(dimensionListener);
		dimensionHeightField.setBackground(Color.WHITE);
		dimensionHeightField.setToolTipText("H\u00F6he der Leinwand");
		shapeBuildPane.add(dimensionHeightField, "cell 0 0,growx");
		
		dimensionWidthField = new JSpinner();
		dimensionWidthField.setModel(new SpinnerNumberModel(new Integer(300), new Integer(0), null, new Integer(1)));
		dimensionWidthField.addChangeListener(dimensionListener);
		dimensionWidthField.setBackground(Color.WHITE);
		dimensionWidthField.setToolTipText("Die Breite der Leinwand");
		shapeBuildPane.add(dimensionWidthField, "cell 0 0,growx");
		
		showDistributionPanelCheckbox = new JCheckBox("Zeige Gleichm\u00E4\u00DFigkeitsrelevante Formen an");
		showDistributionPanelCheckbox.setSelected(true);
		showDistributionPanelCheckbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				distributionPanel.setVisible(showDistributionPanelCheckbox.isSelected());
				showMaxMoveDistanceCheckbox.setEnabled(showDistributionPanelCheckbox.isSelected());
			}
		});
		shapeBuildPane.add(showDistributionPanelCheckbox, "cell 0 3");
		
		showMaxMoveDistanceCheckbox = new JCheckBox("Zeige den maximalen Bewegungsradius an");
		showMaxMoveDistanceCheckbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				distributionPanel.setShowMaxMovement(showMaxMoveDistanceCheckbox.isSelected());
			}
		});
		showMaxMoveDistanceCheckbox.setToolTipText("Zieht einen Kreis mit dem maximalen Bewegungsradius um den Kreis mit dem Minimalabstand");
		shapeBuildPane.add(showMaxMoveDistanceCheckbox, "cell 0 4");
		
		JComponent[] activityDependantComponents = {
				activeLabel,
				moveCirclesRandomlyButton,
				spreadCirclesButton,
				readShapeButton,
				setCirclesButton,
				distributionStopButton,
				spreadSettings,
				distributionMethods,
				silentMutationCheckbox,
				onlyMinDistanceCheckbox,
				distributionMethodOptimal
		};
		
		algMonitor.addComponents(activityDependantComponents);
		
		readPanelSize();
		applyPanelSize();
	}
	
	public void readPanelSize (){
		int width, height;
		try{
			width = (Integer) dimensionHeightField.getValue();
			height = (Integer) dimensionWidthField.getValue();
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(self,
				    "Ung�ltige Eingabe f�r die Leinwanddimensionen",
				    "Ung�ltiger Wert",
				    JOptionPane.ERROR_MESSAGE);
			return;
		}
		panelSize = new Dimension(width, height);
	}

	public void applyPanelSize (){
		Rectangle bounds = new Rectangle(0, 0, (int)getPanelSize().getWidth(), (int)getPanelSize().getHeight());
		setBounds(
				100,
				100,
				100 + tabbedPane.getWidth() + (int)getPanelSize().getWidth(),
				100 + (int)getPanelSize().getHeight()
		);
		layeredPane.setSize(getPanelSize());
		layeredPane.setPreferredSize(getPanelSize());
		JComponent[] panels = {
				distributionPanel,
				circlePanel,
				shapePanel,
				background
		};
		for(JComponent jc : panels){
			jc.setSize(getPanelSize());
			jc.setBounds(bounds);
		}
		pack();
	}
	
	public static Dimension getPanelSize() {
		return panelSize;
	}
	
	private int getDistributionMethod(){
		if(distributionMethodAllCircles.isSelected())
			return EvAlgThread.ALL_CIRCLES_METHOD;
		else if(distributionMethodOneCircle.isSelected())
			return EvAlgThread.MIN_CIRCLE_METHOD;
		else if(distributionMethodBoth.isSelected())
			return EvAlgThread.BOTH_METHODS;
		else if(distributionMethodOptimal.isSelected())
			return EvAlgThread.OPT_MIN_CIRCLE_METHOD;
		
		else
			return EvAlgThread.BOTH_METHODS;
	}
	
	public void updateRegularityValue(){
		if(evAlg != null && progressBar != null){
			double newValue = ((double) evAlg.getDoneSteps() / (double)(Integer) eaStepsSpinner.getValue());
			progressBar.setValue((int) (newValue * 100));
		}
		if(Main.getSynchronizedStore() != null && computeRegularityField != null){
			computeRegularityField.setText(String.valueOf(Main.getSynchronizedStore().computeRegularity()));
		}
	}
}
