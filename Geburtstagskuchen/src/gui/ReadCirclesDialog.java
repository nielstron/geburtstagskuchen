package gui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import computation.Circle;
import computation.Main;
import computation.SynchronizedStore;
import gui.GuiCanvas.ShapeCanvas;
import javax.swing.JRadioButton;
import net.miginfocom.swing.MigLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.event.MouseMotionAdapter;



public class ReadCirclesDialog extends ReadDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6846089498668137911L;
	private JPanel contentPane;
	private ShapeCanvas shapePanel;
	
	private JSpinner radiusField;
	private JPanel options;
	private GuiCanvas canvas;

	private ButtonGroup auswahlGruppe;
	private Set<JRadioButton> auswahlButtons;
	private JRadioButton setCircleButton;
	private JRadioButton moveCircleButton;
	private JRadioButton removeCircleButton;
	private JLabel radiusLabel;
	private JLabel radiusPxLabel;
	private JButton okayButton;
	private JButton cancelButton;
	private JLayeredPane panel;
	private JTextField mouseXPositionField;
	private JTextField mouseYPositionField;
	private JPanel background;
	private JButton removeAlleCirclesButton;
	private JLabel mouseCoordinatesLabel;

	
	
	/**
	 * Create the frame.
	 */
	public ReadCirclesDialog(AppWindow parent) {
		setTitle("Kreise verteilen");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AppWindow.class.getResource("/icon.png")));
		ReadCirclesDialog self = this;
		
		setModal(true);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		syncstore = new SynchronizedStore();
		
		options = new JPanel();
		contentPane.add(options, BorderLayout.WEST);
		options.setLayout(new MigLayout("", "[109px,grow]", "[23px][23px][23px][23px][23px][23px][][23px][23px][23px]"));
		
		auswahlGruppe = new ButtonGroup();
		auswahlButtons = new HashSet<JRadioButton>();
		
		setCircleButton = new JRadioButton("Setze einen Kreis");
		setCircleButton.setSelected(true);
		options.add(setCircleButton, "cell 0 0,growx,aligny top");
		auswahlButtons.add(setCircleButton);
		
		moveCircleButton = new JRadioButton("Bewege einen Kreis");
		options.add(moveCircleButton, "cell 0 1");
		auswahlButtons.add(moveCircleButton);
		
		removeCircleButton = new JRadioButton("Entferne einen Kreis");
		options.add(removeCircleButton, "cell 0 2");
		auswahlButtons.add(removeCircleButton);
		

		//Gruppiere die Auswahlmöglichkeiten
		for(JRadioButton manualInputButton : auswahlButtons){
			auswahlGruppe.add(manualInputButton);
		}
		
		radiusLabel = new JLabel("Kreisradius:");
		options.add(radiusLabel, "flowx,cell 0 3");
		
		okayButton = new JButton("\u00DCbernehmen");
		okayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.getSynchronizedStore().copyFrom(syncstore);
				parent.repaint();
				self.setVisible(false);
			}
		});
		
		JButton manualInputButton = new JButton("Manuelle Eingabe");
		manualInputButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Generiere die Stringrepräsentation der Kreise
				String circles = new String();
				for(computation.Circle c : syncstore.getCircles()){
					circles += c;
				}
				String s = (String)JOptionPane.showInputDialog(
	                    self,
	                    "Geben sie bitte die Werte der Kreise an",
	                    "Manuelle Eingabe",
	                    JOptionPane.PLAIN_MESSAGE,
	                    null,
	                    null,
	                    circles);
				Set<computation.Circle> newCircles = computation.Circle.parseCircles(s);
				for(computation.Circle c : newCircles){
					syncstore.addCircle(c);
				}
				canvas.repaint();
			}
		});
		options.add(manualInputButton, "cell 0 4,growx");
		
		removeAlleCirclesButton = new JButton("Entferne alle Kreise");
		removeAlleCirclesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				syncstore.setCircels(new HashSet<Circle>());
				canvas.repaint();
			}
		});
		options.add(removeAlleCirclesButton, "cell 0 5");
		
		mouseCoordinatesLabel = new JLabel("Koordinaten der Maus:");
		options.add(mouseCoordinatesLabel, "cell 0 6");
		
		mouseXPositionField = new JTextField();
		mouseXPositionField.setText("0");
		mouseXPositionField.setToolTipText("Die aktuelle X-Koordinate der Maus");
		mouseXPositionField.setBackground(Color.WHITE);
		mouseXPositionField.setEditable(false);
		options.add(mouseXPositionField, "flowx,cell 0 7,alignx leading,growy");
		mouseXPositionField.setColumns(10);
		options.add(okayButton, "cell 0 8,growx");
		
		cancelButton = new JButton("Abbrechen");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					resetSyncStore();
				} catch (CloneNotSupportedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				parent.repaint();
				self.setVisible(false);
			}
		});
		options.add(cancelButton, "cell 0 9,growx,aligny baseline");
		
		radiusField = new JSpinner();
		radiusField.setModel(new SpinnerNumberModel(7.0, 0.0, 300.0, 1.0));
		options.add(radiusField, "cell 0 3");
		
		radiusPxLabel = new JLabel("px");
		options.add(radiusPxLabel, "cell 0 3");
		
		mouseYPositionField = new JTextField();
		mouseYPositionField.setToolTipText("Die Aktuelle Y-Koordinate der Maus");
		mouseYPositionField.setText("0");
		mouseYPositionField.setBackground(Color.WHITE);
		mouseYPositionField.setEditable(false);
		options.add(mouseYPositionField, "cell 0 7,alignx trailing,growy");
		mouseYPositionField.setColumns(10);
		
		panel = new JLayeredPane();
		panel.setBackground(new Color(255, 255, 255));
		contentPane.add(panel, BorderLayout.CENTER);
		
		canvas = new GuiCanvas.CirclesCanvas(syncstore);
		canvas.setBackground(new Color(0,0,0,0));
		canvas.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				if(moveCircleButton.isSelected()){
					syncstore.moveCircle(arg0.getPoint(), arg0.getPoint());
					canvas.repaint();
				}
			}
			@Override
			public void mouseMoved(MouseEvent arg0) {
				mouseXPositionField.setText(String.valueOf(arg0.getX()));
				mouseYPositionField.setText(String.valueOf(arg0.getY()));
			}
		});
		panel.setLayout(null);
		panel.add(canvas);
		
		shapePanel = new GuiCanvas.ShapeCanvas();
		shapePanel.setBackground(new Color(0,0,0,0));
		panel.add(shapePanel);
		
		background = new JPanel();
		background.setBackground(Color.WHITE);
		panel.add(background);
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(setCircleButton.isSelected()){
					syncstore.addCircle(arg0.getPoint(), (Double) radiusField.getValue());
				}
				else if(removeCircleButton.isSelected()){
					syncstore.removeCircle(arg0.getPoint());
				}
				canvas.repaint();
			}

		});
		
		updatePanelSize();
		pack();
	}
	
	public void updatePanelSize(){
		Rectangle bounds = new Rectangle(0, 0, (int)AppWindow.getPanelSize().getWidth(), (int)AppWindow.getPanelSize().getHeight());
		JComponent[] panels = {
				panel,
				canvas,
				shapePanel,
				background
		};
		for(JComponent jc : panels){
			jc.setPreferredSize(AppWindow.getPanelSize());
			jc.setSize(AppWindow.getPanelSize());
			jc.setBounds(bounds);
		}
	}

}

