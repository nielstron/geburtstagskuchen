package gui;

import java.awt.BorderLayout;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import computation.FileInput;
import computation.Main;
import computation.Shape;
import computation.SynchronizedStore;
import computation.Triangle;
import computation.Vertex;
import data.DataReference;

import javax.swing.JRadioButton;
import net.miginfocom.swing.MigLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.JTextField;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.net.URL;

import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenuItem;

public class ReadShapeDialog extends ReadDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7252083687336992377L;

	private JPanel contentPane;
	private ReadShapeDialog self;
	
	private JPanel options;
	private JLayeredPane panel;
	private JRadioButton moveVertexButton;
	private JButton okayButton;
	private JRadioButton removeVertexButton;
	private JRadioButton setVertexButton;
	private JPanel background;
	private JButton cancelButton;
	private GuiCanvas shapeCanvas;
	private ButtonGroup auswahlGruppe;
	private JTextPane instructionPane;
	private JTextField mouseXPositionField;
	private JTextField mouseYPositionField;
	
	/**
	 * Radius, in dem Knoten anklickbar sind
	 */
	private int vertexRadius = 10;
	private JButton manualInputButton;
	private JPopupMenu examplesMenu;
	private JMenuItem exampleHeartShape;
	private JMenuItem exampleTriangle;
	private JMenuItem exampleSquare;
	private JMenuItem exampleRectangle;
	private JButton btnAuswahlVergessen;
	private JMenuItem exampleKreis;

	/**
	 * Create the frame.
	 */
	public ReadShapeDialog(AppWindow parent) {
		setTitle("Form setzen");
		self = this;
		setIconImage(Toolkit.getDefaultToolkit().getImage(AppWindow.class.getResource("/icon.png")));
		setModal(true);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		syncstore = new SynchronizedStore();
		
		panel = new JLayeredPane();
		contentPane.add(panel, BorderLayout.CENTER);
		//Größenfestlegung erfolgt später
		panel.setLayout(null);
		
		shapeCanvas = new GuiCanvas.ShapeCanvas(syncstore);
		shapeCanvas.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				mouseXPositionField.setText(String.valueOf(arg0.getX()));
				mouseYPositionField.setText(String.valueOf(arg0.getY()));
			}
			@Override
			public void mouseDragged(MouseEvent e) {
				mouseXPositionField.setText(String.valueOf(e.getX()));
				mouseYPositionField.setText(String.valueOf(e.getY()));
				if(moveVertexButton.isSelected()){
					//TODO Knoten verschieben
				}
			}
		});
		shapeCanvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int n = 0;
				if(arg0.getButton() == MouseEvent.BUTTON1){
					if(setVertexButton.isSelected()){
						n = syncstore.rememberVertex(new Vertex(arg0.getPoint()), vertexRadius);
					}
					else if(removeVertexButton.isSelected()){
						syncstore.removeVertex(arg0.getPoint(), vertexRadius);
					}
				}
				else if(arg0.getButton() == MouseEvent.BUTTON3){
					syncstore.forgetVertices();
				}
				if(!syncstore.getShape().getTriangles().isEmpty()){
					String instruction = "Setzen sie einen neuen Knoten";
					switch(n){
					case 0:
						instruction = "Wählen sie einen bereits gesetzten Knoten aus.";
						break;
					case 1:
						instruction = "Wählen sie einen weiteren bereits gesetzten Knoten aus.";
						break;
					case 2:
						instruction = "Setzen sie einen neuen Knoten oder wählen sie einen weiteren bekannten Knoten aus";
						break;
					}
					instructionPane.setText(instruction);
				}
				shapeCanvas.repaint();
			}
		});
		shapeCanvas.setBackground(new Color(0,0,0,0));
		panel.add(shapeCanvas);
		
		background = new JPanel();
		background.setBackground(Color.WHITE);
		panel.add(background);
		background.setLayout(null);
		
		options = new JPanel();
		contentPane.add(options, BorderLayout.WEST);
		options.setLayout(new MigLayout("", "[109px]", "[23px][23px][23px][23px][23px][23px][23px][grow][23px][23px][23px][23px]"));
		
		auswahlGruppe = new ButtonGroup();
		Set<JRadioButton> auswahlButtons = new HashSet<JRadioButton>();
		
		setVertexButton = new JRadioButton("Setze neues Dreieck");
		setVertexButton.setSelected(true);
		options.add(setVertexButton, "cell 0 0,growx,aligny top");
		auswahlButtons.add(setVertexButton);
		
		moveVertexButton = new JRadioButton("Bewege Knoten");
		moveVertexButton.setEnabled(false);
		options.add(moveVertexButton, "cell 0 1,growx");
		auswahlButtons.add(moveVertexButton);
		
		removeVertexButton = new JRadioButton("Entferne Knoten");
		options.add(removeVertexButton, "cell 0 2,growx");
		auswahlButtons.add(removeVertexButton);
		
		//Gruppiere die Auswahlmöglichkeiten
		for(JRadioButton removeAllVerticesButton : auswahlButtons){
			auswahlGruppe.add(removeAllVerticesButton);
		}
		
		okayButton = new JButton("\u00DCbernehmen");
		okayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.getSynchronizedStore().copyFrom(syncstore);
				parent.repaint();
				self.setVisible(false);
			}
		});
		
		manualInputButton = new JButton("Manuelle Eingabe");
		manualInputButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Generiere die Stringrepräsentation der Kreise
				String points = new String();
				for(computation.Triangle t : syncstore.getShape().getTriangles()){
					points += t;
				}
				String s = (String)JOptionPane.showInputDialog(
	                    self,
	                    "Geben sie bitte die Werte der Dreiecke an\n" +
	                    "Überschneidungen werden nicht überprüft!",
	                    "Manuelle Eingabe",
	                    JOptionPane.PLAIN_MESSAGE,
	                    null,
	                    null,
	                    points);
				Set<Triangle> triangles = Triangle.parseTriangles(s);
				syncstore.getShape().getTriangles().addAll(triangles);
				shapeCanvas.repaint();
			}
		});
		options.add(manualInputButton, "cell 0 3,growx");
		
		examplesMenu = new JPopupMenu();
		addPopup(manualInputButton, examplesMenu);
		
		exampleTriangle = new JMenuItem("Dreieck");
		exampleTriangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readShapeFromFile("dreieckform.txt");
			}
		});
		examplesMenu.add(exampleTriangle);
		
		exampleSquare = new JMenuItem("Quadrat");
		exampleSquare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readShapeFromFile("quadratform.txt");
			}
		});
		examplesMenu.add(exampleSquare);
		
		exampleRectangle = new JMenuItem("Rechteck");
		exampleRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readShapeFromFile("rechteckform.txt");
			}
		});
		examplesMenu.add(exampleRectangle);
		
		exampleHeartShape = new JMenuItem("Herzform");
		exampleHeartShape.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				readShapeFromFile("herzform.txt");
			}
		});
		
		exampleKreis = new JMenuItem("Kreis");
		exampleKreis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				readShapeFromFile("kreisform.txt");
			}
		});
		examplesMenu.add(exampleKreis);
		examplesMenu.add(exampleHeartShape);
		
		JButton removeAllVerticesButton = new JButton("Entferne alle Knoten");
		removeAllVerticesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				syncstore.setShape(new Shape());
				shapeCanvas.repaint();
			}
		});
		options.add(removeAllVerticesButton, "cell 0 4,growx");
		
		btnAuswahlVergessen = new JButton("Auswahl vergessen");
		btnAuswahlVergessen.setToolTipText("Vergisst die aktuell ausgew\u00E4hlten Eckpunkte. Kann \u00FCber die RECHTE MAUSTASTE aktiviert werden.");
		btnAuswahlVergessen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				syncstore.forgetVertices();
				shapeCanvas.repaint();
			}
		});
		options.add(btnAuswahlVergessen, "cell 0 5,growx");
		
		instructionPane = new JTextPane();
		instructionPane.setBackground(UIManager.getColor("Button.background"));
		instructionPane.setToolTipText("Ein Tipp f\u00FCr den n\u00E4chsten Schritt");
		instructionPane.setText("Setzen sie einen Punkt");
		options.add(instructionPane, "flowy,cell 0 7");
		
		JLabel mouseCoordinatesLabel = new JLabel("Koordinaten der Maus:");
		options.add(mouseCoordinatesLabel, "cell 0 8");
		
		mouseXPositionField = new JTextField();
		options.add(mouseXPositionField, "flowx,cell 0 9");
		mouseXPositionField.setToolTipText("Die aktuelle X-Koordinate der Maus");
		mouseXPositionField.setText("0");
		mouseXPositionField.setEditable(false);
		mouseXPositionField.setColumns(10);
		mouseXPositionField.setBackground(Color.WHITE);
		options.add(okayButton, "cell 0 10,growx");
		
		cancelButton = new JButton("Abbrechen");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					resetSyncStore();
				} catch (CloneNotSupportedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				parent.repaint();
				self.setVisible(false);
			}
		});
		options.add(cancelButton, "cell 0 11,growx,aligny baseline");
		
		mouseYPositionField = new JTextField();
		options.add(mouseYPositionField, "cell 0 9");
		mouseYPositionField.setToolTipText("Die Aktuelle Y-Koordinate der Maus");
		mouseYPositionField.setText("0");
		mouseYPositionField.setEditable(false);
		mouseYPositionField.setColumns(10);
		mouseYPositionField.setBackground(Color.WHITE);
		
		updatePanelSize();
		pack();
	}
	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	
	/**
	 * Liest die Form aus, die in einer Datei mit dem übergebenen Namen im Dateiordner der Klasse "ReadShape" liegt
	 * @param filename Datei mit dem übergebenen Namen, die im Dateiordner der Klasse "ReadShape" liegt
	 */
	public void readShapeFromFile(String filename){
		syncstore.setShape(new Shape());
		URL url = DataReference.class.getResource(filename);
		File heartFile = new File(url.getPath().replace("%20", " "));
		String heartString = FileInput.fileToString(heartFile);
		Set<Triangle> triangles = Triangle.parseTriangles(heartString);
		syncstore.getShape().getTriangles().addAll(triangles);
		shapeCanvas.repaint();
	}
	
	public void updatePanelSize(){
		Rectangle bounds = new Rectangle(0, 0, (int)AppWindow.getPanelSize().getWidth(), (int)AppWindow.getPanelSize().getHeight());
		JComponent[] panels = {
				panel,
				shapeCanvas,
				background
		};
		for(JComponent jc : panels){
			jc.setPreferredSize(AppWindow.getPanelSize());
			jc.setSize(AppWindow.getPanelSize());
			jc.setBounds(bounds);
		}
	}
	
}
