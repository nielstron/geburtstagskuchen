package gui;

import javax.swing.JDialog;

import computation.Main;
import computation.SynchronizedStore;

public abstract class ReadDialog extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2981060436748507754L;
	
	/**
	 * Lager, zum speichern der aktuell eingegebenen Werte
	 */
	protected SynchronizedStore syncstore = new SynchronizedStore();

	public void resetSyncStore() throws CloneNotSupportedException{
		syncstore.forgetVertices();
		syncstore.copyFrom(Main.getSynchronizedStore());
	}
	
	//Eine Methode um die Inhalte des DIaloges vor dem Aufruf noch einmal zu erneuern
	public abstract void updatePanelSize();
	
	public void setVisible (boolean b){
		updatePanelSize();
		pack();
		if(b)
			try {
				resetSyncStore();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		super.setVisible(b);
	}
}
