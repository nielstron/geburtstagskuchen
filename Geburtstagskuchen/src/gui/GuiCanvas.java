package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Line2D;
import java.util.ConcurrentModificationException;

import javax.swing.JComponent;

import computation.Circle;
import computation.Main;
import computation.SynchronizedStore;
import computation.Triangle;
import computation.Vertex;

public class GuiCanvas extends JComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6376622291570829640L;
	
	/**
	 * Lager f�r alles was auf diese Leinwand gemalt werden soll
	 */
	protected SynchronizedStore synchronizedStore;
	
	public GuiCanvas(){
		this(Main.getSynchronizedStore());
	}
	
	public GuiCanvas(SynchronizedStore synchronizedStore){
		this.synchronizedStore = synchronizedStore;
	}

	/**
	 * Das Canvas, auf dem die neue Form gesetzt und ver�ndert werden kann
	 * @author Niels
	 *
	 */
	public static class ShapeCanvas extends GuiCanvas {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2156275404120438514L;
		
		public ShapeCanvas(){
			super();
		}
		
		public ShapeCanvas(SynchronizedStore synchronizedStore){
			super(synchronizedStore);
		}
		
		public void paint(Graphics g){
			//Male alle Dreiecke ...
			for(Triangle t : synchronizedStore.getShape().getTriangles()){
				g.setColor(new Color(255, 200, 200, 200));
				g.fillPolygon(t);
				g.drawPolygon(t);
				g.setColor(new Color(255, 200, 200, 255));
				for(Vertex v : t.getVertices()){
					int rectDim = 5;
					int x = (int) v.getCoords().getX() - rectDim/2;
					int y = (int) v.getCoords().getY() - rectDim/2;
					g.fillRect(x, y, rectDim, rectDim);
				}
			}
			//... alle Knoten der Form, ...
			g.setColor(new Color(200, 255, 200, 255));
			for(Vertex v : synchronizedStore.getRememberedVertices()){
				int rectDim = 5;
				int x = (int) v.getCoords().getX() - rectDim/2;
				int y = (int) v.getCoords().getY() - rectDim/2;
				g.fillRect(x, y, rectDim, rectDim);;
			}
			//... und den Rand der Form
			g.setColor(new Color(0, 0, 0, 255));
			for(Line2D l : synchronizedStore.getShape().getOutline()){
				g.drawLine(
						(int) l.getP1().getX(),
						(int) l.getP1().getY(),
						(int) l.getP2().getX(),
						(int) l.getP2().getY());
			}
			
		}
		
	}
	
	public static class CirclesCanvas extends GuiCanvas {

		/**
		 * 
		 */
		private static final long serialVersionUID = -2110183086070203125L;
		
		public CirclesCanvas(){
			super();
		}
		
		public CirclesCanvas (SynchronizedStore synchronizedStore){
			super(synchronizedStore);
		}
		
		public void paint(Graphics g){
			for(Circle c : synchronizedStore.getCircles()){
				//Male die Kreise
				g.setColor(new Color(200, 200, 255, 200));
				// d ist der durchmesser, NICHT der radius
				int d =(int) c.getRadius() * 2;
				int x =(int) c.getMidpoint().getCoords().getX() - (d/2);
				int y =(int) c.getMidpoint().getCoords().getY() - (d/2);
				g.fillOval(x, y, d, d);
				//Und die Mittelpunkte
				g.setColor(new Color(100, 100, 255, 255));
				d = 4;
				x = (int) c.getMidpoint().getCoords().getX() - (d/2);
				y = (int) c.getMidpoint().getCoords().getY() - (d/2);
				g.fillOval(x, y, d, d);
			}
		}
	}
	
	public static class DistributionShapesCanvas extends GuiCanvas {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5057611494099648808L;
		
		private boolean showMaxMovement;
		
		public void paint(Graphics g){
			g.setColor(new Color(200, 255, 200, 255));
			Line2D l = null;
			try{
				l = synchronizedStore.shortestConnectingLine();
			}catch(ConcurrentModificationException e){
				e.printStackTrace();
				//TODO warum?..
			}
			if(l != null){
				//Male eine Linie vom Kreis mit Minimalabstand zum n�chsten Kreis oder Rand
				g.drawLine(
						(int) l.getP1().getX(),
						(int) l.getP1().getY(),
						(int) l.getP2().getX(),
						(int) l.getP2().getY());
				g.setColor(new Color(200, 255, 200, 200));
				//Male einen Kreis mit der Minimaldistanz um jeden Kreis
				double minimalDistance = synchronizedStore.minimalDistance();
				for(Circle c : synchronizedStore.getCircles()){
					// d ist der durchmesser, NICHT der radius
					int d =(int) (c.getRadius() * 2 + minimalDistance );
					int x =(int) c.getMidpoint().getCoords().getX() - (d/2);
					int y =(int) c.getMidpoint().getCoords().getY() - (d/2);
					g.drawOval(x, y, d, d);
				}
				//Male um den Kreis mit dem Minimalabstand einen Kreis mit der maximalen Bewegungsdistanz
				if(showMaxMovement){
					double maxMoveDistance = (synchronizedStore.optimalDistance() - minimalDistance) * 4;
					int d = (int) maxMoveDistance;
					int x = (int) l.getP2().getX()  - (d/2);
					int y =	(int) l.getP2().getY()  - (d/2);
					g.setColor(new Color(200, 200, 255, 100));
					g.fillOval(x, y, d, d);
				}
			}
		}

		public boolean doesShowMaxMovement() {
			return showMaxMovement;
		}

		public void setShowMaxMovement(boolean showMaxMovement) {
			this.showMaxMovement = showMaxMovement;
			repaint();
		}
		
		
	}
}
